import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from './shared/components';
import { AuthGuardService } from './shared/services';
import { HomeComponent } from './pages/home/home.component';
import {
    DxButtonModule,
    DxCheckBoxModule,
    DxDataGridModule,
    DxDateBoxModule,
    DxDropDownButtonModule,
    DxFileUploaderModule,
    DxFormModule,
    DxListModule,
    DxLoadPanelModule,
    DxNumberBoxModule,
    DxPopupModule,
    DxResponsiveBoxModule,
    DxScrollViewModule,
    DxSelectBoxModule,
    DxTabPanelModule,
    DxTagBoxModule,
    DxTextBoxModule
} from 'devextreme-angular';
import { ItemCategoryComponent } from './pages/item-category/item-category.component';
import { ItemSubcategoryComponent } from './pages/item-subcategory/item-subcategory.component';
import { ItemTagComponent } from './pages/item-tag/item-tag.component';
import { CustomerCategoryComponent } from './pages/customer-category/customer-category.component';
import { SupplierCategoryComponent } from './pages/supplier-category/supplier-category.component';
import { UnitComponent } from './pages/unit/unit.component';
import { ItemComponent } from './pages/item/item.component';
import { PaymentMethodComponent } from './pages/payment-method/payment-method.component';
import { CustomerComponent } from './pages/customer/customer.component';
import { SupplierComponent } from './pages/supplier/supplier.component';
import { PriceControlCategoryComponent } from './pages/price-control-category/price-control-category.component';
import { StockControlCategoryComponent } from './pages/stock-control-category/stock-control-category.component';
import { LocationComponent } from './pages/location/location.component';
import { RoleComponent } from './pages/role/role.component';
import { PriceControlComponent } from './pages/price-control/price-control.component';
import { StockControlComponent } from './pages/stock-control/stock-control.component';
import { BillOfMaterialComponent } from './pages/bill-of-material/bill-of-material.component';
import { BillOfMaterialEditComponent } from './pages/bill-of-material-edit/bill-of-material-edit.component';
import { UserComponent } from './pages/user/user.component';
import { PurchaseOrderComponent } from './pages/purchase-order/purchase-order.component';
import { PurchaseOrderEditComponent } from './pages/purchase-order-edit/purchase-order-edit.component';
import { SectionComponent } from './pages/section/section.component';
import { TaxComponent } from './pages/tax/tax.component';

const routes: Routes = [
    {
        path: 'tax',
        component: TaxComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'section',
        component: SectionComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'purchase-order/:mode',
        component: PurchaseOrderEditComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'purchase-order/:mode/:id',
        component: PurchaseOrderEditComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'purchase-order',
        component: PurchaseOrderComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'user',
        component: UserComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'bill-of-material/:mode',
        component: BillOfMaterialEditComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'bill-of-material/:mode/:id',
        component: BillOfMaterialEditComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'bill-of-material',
        component: BillOfMaterialComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'stock-control',
        component: StockControlComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'price-control',
        component: PriceControlComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'role',
        component: RoleComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'location',
        component: LocationComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'stock-control-category',
        component: StockControlCategoryComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'price-control-category',
        component: PriceControlCategoryComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'supplier',
        component: SupplierComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'customer',
        component: CustomerComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'payment-method',
        component: PaymentMethodComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'item',
        component: ItemComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'unit',
        component: UnitComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'supplier-category',
        component: SupplierCategoryComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'customer-category',
        component: CustomerCategoryComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'item-tag',
        component: ItemTagComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'item-subcategory',
        component: ItemSubcategoryComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'item-category',
        component: ItemCategoryComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'login',
        component: LoginFormComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: '**',
        redirectTo: 'home',
        canActivate: [AuthGuardService]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
        CommonModule,
        DxDataGridModule,
        DxFormModule,
        DxButtonModule,
        DxDropDownButtonModule,
        DxLoadPanelModule,
        DxResponsiveBoxModule,
        DxTagBoxModule,
        DxPopupModule,
        DxTabPanelModule,
        DxSelectBoxModule,
        DxCheckBoxModule,
        DxNumberBoxModule,
        DxDateBoxModule,
        DxListModule,
        DxScrollViewModule,
        DxTextBoxModule,
        DxFileUploaderModule,
        DxTagBoxModule
    ],
    providers: [AuthGuardService],
    exports: [RouterModule],
    declarations: [
        HomeComponent,
        ItemCategoryComponent,
        ItemSubcategoryComponent,
        ItemTagComponent,
        CustomerCategoryComponent,
        SupplierCategoryComponent,
        UnitComponent,
        ItemComponent,
        PaymentMethodComponent,
        CustomerComponent,
        SupplierComponent,
        PriceControlCategoryComponent,
        StockControlCategoryComponent,
        LocationComponent,
        RoleComponent,
        PriceControlComponent,
        StockControlComponent,
        BillOfMaterialComponent,
        BillOfMaterialEditComponent,
        UserComponent,
        PurchaseOrderComponent,
        PurchaseOrderEditComponent,
        SectionComponent,
        TaxComponent
    ]
})
export class AppRoutingModule { }
