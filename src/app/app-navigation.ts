export const navigation = [
    {
        text: 'Home',
        path: '/home',
        icon: 'home'
    },
    {
        text: 'Data',
        icon: 'folder',
        items: [
            {
                text: 'Item',
                path: '/item'
            },
            {
                text: 'Bill of Material',
                path: '/bill-of-material'
            },
            {
                text: 'Price Control',
                path: '/price-control'
            },
            {
                text: 'Stock Control',
                path: '/stock-control'
            },
            {
                text: 'Supplier',
                path: '/supplier'
            },
            {
                text: 'Customer',
                path: '/customer'
            },
            {
                text: 'Payment Method',
                path: '/payment-method'
            },
            {
                text: 'Tax',
                path: '/tax'
            },
            {
                text: 'Location',
                path: '/location'
            },
            {
                text: 'Section',
                path: '/section'
            },
            {
                text: 'User',
                path: '/user'
            }
        ],
    },
    {
        text: 'Inventory',
        icon: 'folder',
        items: [
        ]
    },
    {
        text: 'Purchasing',
        icon: 'folder',
        items: [
            {
                text: 'Purchase Order',
                path: '/purchase-order'
            },
        ]
    },
    {
        text: 'Sales',
        icon: 'folder',
        items: [

        ]
    },
    {
        text: 'Accounting',
        icon: 'folder',
        items: [
        ]
    },
    {
        text: 'Setting',
        icon: 'folder',
        items: [

        ]
    },
];
