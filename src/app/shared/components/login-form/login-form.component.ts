import { CommonModule } from '@angular/common';
import { Component, NgModule } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxLoadIndicatorModule } from 'devextreme-angular/ui/load-indicator';
import notify from 'devextreme/ui/notify';
import { AuthService } from '../../services';
import { UserService } from '../../services/user.service';


@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {
    loading = false;
    formData: any = {};

    constructor(private authService: AuthService,
        private router: Router) { }

    async onSubmit(e) {
        try {
            e.preventDefault();
            const { userName, password } = this.formData;
            this.loading = true;

            await this.authService.login(userName, password);
        } catch (error) {
            this.loading = false;
            notify(error.error.message, 'error', 2000);
        }
    }
}
@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        DxFormModule,
        DxLoadIndicatorModule
    ],
    declarations: [LoginFormComponent],
    exports: [LoginFormComponent]
})
export class LoginFormModule { }
