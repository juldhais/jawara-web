import { Component, NgModule, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { confirm } from 'devextreme/ui/dialog';

import { AuthService } from '../../services';
import { DxButtonModule } from 'devextreme-angular/ui/button';
import { DxToolbarModule } from 'devextreme-angular/ui/toolbar';

import { Router } from '@angular/router';
import { Helper } from '../../helpers/helper';
@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
    @Output()
    menuToggle = new EventEmitter<boolean>();

    @Input()
    menuToggleEnabled = false;

    @Input()
    title: string;

    user = { userName: '' };

    constructor(private authService: AuthService, private router: Router) { }

    ngOnInit() {
        this.user = Helper.getIdentity().user;
    }

    toggleMenu = () => {
        this.menuToggle.emit();
    }

    async logout() {
        const result = await confirm('Are you sure want to logout?', 'LOGOUT');
        if (result) {
            Helper.removeLocalStorageItem('identity');
            this.router.navigateByUrl('login');
        }
    }
}

@NgModule({
    imports: [
        CommonModule,
        DxButtonModule,
        DxToolbarModule
    ],
    declarations: [HeaderComponent],
    exports: [HeaderComponent]
})
export class HeaderModule { }
