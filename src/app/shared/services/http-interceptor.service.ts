import { Injectable } from '@angular/core';
import { Observable, of, throwError } from "rxjs";
import { Router } from '@angular/router';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest
} from '@angular/common/http';
import { Helper } from '../helpers/helper';

@Injectable({
    providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

    constructor(private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const identity = Helper.getIdentity();
        const token = identity ? identity.token : '';

        req = req.clone({
            url: `${globalThis.baseUrl}/${req.url}`,
            setHeaders: { Authorization: `Bearer ${token}` }
        });

        return next.handle(req);
    }
}
