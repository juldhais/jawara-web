import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigurationResource } from '../resources/configuration.resource';
import { ConfigurationEditResource } from '../resources/configuration-edit.resource';

@Injectable({
    providedIn: 'root'
})
export class ConfigurationService {

    constructor(private http: HttpClient) { }

    get(key) {
        const url = `configuration/${key}`;
        return this.http.get<ConfigurationResource>(url).toPromise();
    }

    set(resource: ConfigurationResource) {
        const url = `configuration`;
        return this.http.post(url, resource).toPromise();
    }

    getEdit() {
        const url = `configuration/get`;
        return this.http.get<ConfigurationEditResource>(url).toPromise();
    }

    setEdit(resource: ConfigurationEditResource) {
        const url = `configuration/set`;
        return this.http.post(url, resource).toPromise();
    }
}
