import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { SalesDeliveryResource } from '../resources/sales-delivery.resource';
import { PagingResult } from '../resources/paging-result.resource';
import { Helper } from '../helpers/helper';
import { PrintResource } from '../resources/print.resource';
import { ErrorHelper } from '../helpers/error-helper';
import { ScalarResource } from '../resources/scalar.resource';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';

@Injectable({
    providedIn: 'root'
})
export class SalesDeliveryService {

    constructor(private http: HttpClient) { }

    get(id) {
        const url = `sales-delivery/${id}`;
        return this.http.get<SalesDeliveryResource>(url).toPromise();
    }

    getList(params?: {
        keyword?: string, 
        status?: string, 
        customerId?: string, 
        locationId?: string, 
        startDate?: Date, 
        endDate?: Date, 
        page?: number, 
        size?: number
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `sales-delivery`;
        return this.http.get<PagingResult<SalesDeliveryResource>>(url, { params: httpParams }).toPromise();
    }

    create(resource: SalesDeliveryResource) {
        const url = `sales-delivery`;
        return this.http.post<ScalarResource>(url, resource).toPromise();
    }

    update(resource: SalesDeliveryResource) {
        const url = `sales-delivery/${resource.id}`;
        return this.http.put<ScalarResource>(url, resource).toPromise();
    }

    createOrUpdate(resource: SalesDeliveryResource, mode: string) {
        if (mode == 'create')
            return this.create(resource);
        else if (mode == 'update')
            return this.update(resource);
    }

    delete(id) {
        const url = `sales-delivery/${id}`;
        return this.http.delete(url).toPromise();
    }

    print(id) {
        const url = `sales-delivery/print/${id}`;
        return this.http.get<PrintResource>(url).toPromise();
    }

    
    getCustomStore(filter?: {
        keyword?: string,
        status?: string,
        customerId?: string,
        locationId?: string,
        startDate?: Date,
        endDate?: Date
    }) {
        return new CustomStore({
            key: 'id',
            load: (loadOptions) => {
                const skip = loadOptions['skip'];
                const size = loadOptions['take'];
                let page = Math.floor(skip / size) + 1;
                if (isNaN(page) || page == null) page = 0;

                const params = {
                    keyword: filter.keyword,
                    status: filter.status,
                    customerId: filter.customerId,
                    locationId: filter.locationId,
                    startDate: filter.startDate,
                    endDate: filter.endDate,
                    page: page, size: size
                };

                return new Promise((resolve, reject) => {
                    this.getList(params)
                        .then(result => {
                            resolve({
                                data: result.data,
                                totalCount: result.totalData
                            });
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            },
            remove: (key) => {
                return new Promise((resolve, reject) => {
                    this.delete(key)
                        .then(() => {
                            notify('Data deleted successfully.', 'success', 2000);
                            resolve();
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            }
        });
    }
}
