import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ItemResource } from '../resources/item.resource';
import { PagingResult as PagingResult } from '../resources/paging-result.resource';
import { Helper } from '../helpers/helper';
import { ItemStockResource } from '../resources/item-stock.resource';
import { ItemUnitResource } from '../resources/item-unit.resource';
import { SalesPriceResource } from '../resources/sales-price.resource';
import { ScalarResource } from '../resources/scalar.resource';
import { ImportResultResource } from '../resources/import-result.resource';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { LookupResource } from '../resources/lookup.resource';

@Injectable({
    providedIn: 'root'
})
export class ItemService {

    constructor(private http: HttpClient) { }

    get(id) {
        const url = `item/${id}`;
        return this.http.get<ItemResource>(url).toPromise();
    }

    getByCode(code) {
        const url = `item/code/${code}`;
        return this.http.get<ItemResource>(url).toPromise();
    }

    getList(params?: {
        keyword?: string,
        page?: number,
        size?: number
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `item`;
        return this.http.get<PagingResult<ItemResource>>(url, { params: httpParams }).toPromise();
    }

    getListPurchasable(params: {
        keyword?: string,
        page?: number,
        size?: number
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `item/purchasable`;
        return this.http.get<PagingResult<ItemResource>>(url, { params: httpParams }).toPromise();
    }

    getListStockable(params: {
        keyword?: string,
        page?: number,
        size?: number
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `item/stockable`;
        return this.http.get<PagingResult<ItemResource>>(url, { params: httpParams }).toPromise();
    }

    getListManufacturable(params: {
        keyword?: string,
        page?: number,
        size?: number
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `item/manufacturable`;
        return this.http.get<PagingResult<ItemResource>>(url, { params: httpParams }).toPromise();
    }

    getListSellable(params: {
        keyword?: string,
        page?: number,
        size?: number
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `item/sellable`;
        return this.http.get<PagingResult<ItemResource>>(url, { params: httpParams }).toPromise();
    }

    getLookup(params?: { 
        keyword?: string
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `item/lookup`;
        return this.http.get<LookupResource[]>(url, { params: httpParams }).toPromise();
    }

    getSingleLookup(id) {
        const url = `item/lookup/${id}`;
        return this.http.get<LookupResource>(url).toPromise();
    }

    getLookupStockable(params?: { 
        keyword?: string
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `item/lookup/stockable`;
        return this.http.get<LookupResource[]>(url, { params: httpParams }).toPromise();
    }

    getLookupPurchasable(params?: { 
        keyword?: string
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `item/lookup/purchasable`;
        return this.http.get<LookupResource[]>(url, { params: httpParams }).toPromise();
    }

    getLookupSellable(params?: { 
        keyword?: string
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `item/lookup/sellable`;
        return this.http.get<LookupResource[]>(url, { params: httpParams }).toPromise();
    }

    getLookupManufacturable(params?: { 
        keyword?: string
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `item/lookup/manufacturable`;
        return this.http.get<LookupResource[]>(url, { params: httpParams }).toPromise();
    }

    create(resource: ItemResource) {
        const url = `item`;
        return this.http.post<ScalarResource>(url, resource).toPromise();
    }

    update(resource: ItemResource) {
        const url = `item/${resource.id}`;
        return this.http.put<ScalarResource>(url, resource).toPromise();
    }

    patchUpdate(id: string, resource: any) {
        const url = `item/${id}`;
        return this.http.patch<ScalarResource>(url, resource).toPromise();;
    }

    createOrUpdate(resource: ItemResource, mode: string) {
        if (mode == 'create')
            return this.create(resource);
        else if (mode == 'update')
            return this.update(resource);
    }

    delete(id) {
        const url = `item/${id}`
        return this.http.delete(url).toPromise();
    }

    getStock(params: {
        itemId?: string,
        locationId?: string,
        page?: number,
        size?: number
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `item/stock`;
        return this.http.get<PagingResult<ItemStockResource>>(url, { params: httpParams }).toPromise();
    }

    getSalesPrice(resource: SalesPriceResource) {
        const url = `item/sales-price`;
        return this.http.post<ScalarResource>(url, resource).toPromise();
    }

    getUnits(resource: ItemResource) {
        const listUnit: ItemUnitResource[] = [];

        const baseUnit = new ItemUnitResource();
        baseUnit.unit = resource.unit;
        baseUnit.ratio = 1;
        listUnit.push(baseUnit);

        resource.units.forEach(x => listUnit.push(x));

        return listUnit;
    }

    import(file: File) {
        let x = this.getCustomStore();
        const url = `item/import`;
        const formData = new FormData();
        formData.append('file', file, file.name);
        return this.http.post<ImportResultResource>(url, formData).toPromise();
    }

    getCustomStore(type?: string) {
        return new CustomStore({
            key: 'id',
            load: (loadOptions) => {
                const skip = loadOptions.skip;
                const size = loadOptions.take;
                let page = Math.floor(skip / size) + 1;
                if (isNaN(page) || page == null) page = 0;

                let keyword = '';
                if (loadOptions.searchValue)
                    keyword = loadOptions.searchValue;
                else if (loadOptions.filter)
                    keyword = loadOptions.filter[0].filterValue;

                const params = {
                    keyword: keyword,
                    page: page,
                    size: size
                };

                return new Promise((resolve, reject) => {
                    let promise: Promise<PagingResult<ItemResource>>;

                    if (type == 'purchasable') 
                        promise = this.getListPurchasable(params);
                    else if (type == 'stockable') 
                        promise = this.getListStockable(params);
                    else if (type == 'sellable')
                        promise = this.getListSellable(params);
                    else
                        promise = this.getList(params);

                    promise.then(result => {
                        resolve({
                            data: result.data,
                            totalCount: result.totalData
                        });
                    }, error => {
                        if ('error' in error) reject(error.error.message);
                        else reject(error.message);
                    });
                });
            },
            byKey: (key) => {
                return this.get(key);
            },
            insert: (values) => {
                return new Promise((resolve, reject) => {
                    return this.create(values)
                        .then(result => {
                            notify('Data saved successfully.', 'success', 2000);
                            resolve(result);
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });

            },
            update: (key, values) => {
                return new Promise((resolve, reject) => {
                    this.patchUpdate(key, values)
                        .then(result => {
                            notify('Data saved successfully.', 'success', 2000);
                            resolve(result);
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            },
            remove: (key) => {
                return new Promise((resolve, reject) => {
                    this.delete(key)
                        .then(() => {
                            notify('Data deleted successfully.', 'success', 2000);
                            resolve();
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            }
        });
    }

    getLookupCustomStore(type?: string) {
        return new CustomStore({
            key: 'id',
            load: (loadOptions) => {
                const keyword = loadOptions.searchValue;
                    if (!keyword || keyword.length < 2) return;

                const params = { keyword: keyword };

                return new Promise((resolve, reject) => {
                    let promise: Promise<LookupResource[]>;

                    if (type == 'purchasable')
                        promise = this.getLookupPurchasable(params);
                    else if (type == 'stockable')
                        promise = this.getLookupStockable(params);
                    else if (type == 'manufacturable')
                        promise = this.getLookupManufacturable(params);
                    else if (type == 'sellable')
                        promise = this.getLookupSellable(params);
                    else
                        promise = this.getLookup(params);

                    promise.then(result => {
                        resolve({
                            data: result,
                            totalCount: result.length
                        });
                    }, error => {
                        if ('error' in error) reject(error.error.message);
                        else reject(error.message);
                    });
                });
            },
            byKey: (key) => {
                return this.getSingleLookup(key);
            }
        });
    }
}
