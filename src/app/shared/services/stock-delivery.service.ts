import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { StockDeliveryResource } from '../resources/stock-delivery.resource';
import { PagingResult } from '../resources/paging-result.resource';
import { Helper } from '../helpers/helper';
import { ScalarResource } from '../resources/scalar.resource';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { LookupResource } from '../resources/lookup.resource';

@Injectable({
    providedIn: 'root'
})
export class StockDeliveryService {

    constructor(private http: HttpClient) { }

    get(id) {
        const url = `stock-delivery/${id}`;
        return this.http.get<StockDeliveryResource>(url).toPromise();
    }

    getList(params?: {
        keyword?: string, 
        status?: string, 
        destinationId?: string, 
        sourceId?: string, 
        startDate?: Date, 
        endDate?: Date, 
        page?: number, 
        size?: number
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `stock-delivery`;
        return this.http.get<PagingResult<StockDeliveryResource>>(url, { params: httpParams }).toPromise();
    }

    getLookup(params?: { 
        keyword?: string,
        status?: string,
        sourceId?: string,
        destinationId?: string
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `stock-delivery/lookup`;
        return this.http.get<LookupResource[]>(url, { params: httpParams }).toPromise();
    }

    create(resource: StockDeliveryResource) {
        const url = `stock-delivery`;
        return this.http.post<ScalarResource>(url, resource).toPromise();
    }

    update(resource: StockDeliveryResource) {
        const url = `stock-delivery/${resource.id}`;
        return this.http.put<ScalarResource>(url, resource).toPromise();
    }

    createOrUpdate(resource: StockDeliveryResource, mode: string) {
        if (mode == 'create')
            return this.create(resource);
        else if (mode == 'update')
            return this.update(resource);
    }

    delete(id) {
        const url = `stock-delivery/${id}`;
        return this.http.delete(url).toPromise();
    }

    open(id) {
        const url = `stock-delivery/open`;
        return this.http.post(url, { id: id }).toPromise();
    }


    getCustomStore(filter?: {
        keyword?: string, 
        status?: string, 
        destinationId?: string, 
        sourceId?: string, 
        startDate?: Date, 
        endDate?: Date
    }) {
        return new CustomStore({
            key: 'id',
            load: (loadOptions) => {
                const skip = loadOptions['skip'];
                const size = loadOptions['take'];
                let page = Math.floor(skip / size) + 1;
                if (isNaN(page) || page == null) page = 0;

                const params = {
                    keyword: filter?.keyword,
                    status: filter?.status,
                    destinationId: filter?.destinationId,
                    sourceId: filter?.sourceId,
                    startDate: filter?.startDate,
                    endDate: filter?.endDate,
                    page: page, size: size
                };

                return new Promise((resolve, reject) => {
                    this.getList(params)
                        .then(result => {
                            resolve({
                                data: result.data,
                                totalCount: result.totalData
                            });
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            },
            remove: (key) => {
                return new Promise((resolve, reject) => {
                    this.delete(key)
                        .then(() => {
                            notify('Data deleted successfully.', 'success', 2000);
                            resolve();
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            }
        });
    }

    getLookupCustomStore(filter?: { 
        keyword?: string,
        status?: string,
        sourceId?: string,
        destinationId?: string
    }) {
        return new CustomStore({
            key: 'id',
            load: (loadOptions) => {
                const keyword = loadOptions.searchValue;
                    if (!keyword || keyword.length < 2) return;

                const params = { 
                    keyword: keyword, 
                    status: filter?.status, 
                    sourceId: filter?.sourceId, 
                    destinationId: filter?.destinationId 
                };

                return new Promise((resolve, reject) => {
                    this.getLookup(params)
                        .then(result => {
                            resolve({
                                data: result,
                                totalCount: result.length
                            });
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            },
            byKey: (key) => {
                return this.get(key);
            }
        });
    }
}
