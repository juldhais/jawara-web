import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StockControlResource } from '../resources/stock-control.resource';
import { PagingResult } from '../resources/paging-result.resource';
import { ScalarResource } from '../resources/scalar.resource';
import { ImportResultResource } from '../resources/import-result.resource';
import { Helper } from '../helpers/helper';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';

@Injectable({
    providedIn: 'root'
})
export class StockControlService {

    constructor(private http: HttpClient) { }

    get(id) {
        const url = `stock-control/${id}`;
        return this.http.get<StockControlResource>(url).toPromise();
    }

    getList(params?: {
        stockControlCategoryId?: string,
        itemId?: string,
        page?: number,
        size?: number
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `stock-control`;
        return this.http.get<PagingResult<StockControlResource>>(url, { params: httpParams }).toPromise();
    }

    create(resource: StockControlResource) {
        const url = `stock-control`;
        return this.http.post<ScalarResource>(url, resource).toPromise();
    }

    update(resource: StockControlResource) {
        const url = `stock-control/${resource.id}`;
        return this.http.put<ScalarResource>(url, resource).toPromise();
    }

    patchUpdate(id: string, resource: any) {
        const url = `stock-control/${id}`;
        return this.http.patch<ScalarResource>(url, resource).toPromise();;
    }

    createOrUpdate(resource: StockControlResource, mode: string) {
        if (mode == 'create')
            return this.create(resource);
        else if (mode == 'update')
            return this.update(resource);
    }

    delete(id) {
        const url = `stock-control/${id}`
        return this.http.delete(url).toPromise();
    }

    import(file: File) {
        const url = `stock-control/import`;
        const formData = new FormData();
        formData.append('file', file, file.name);
        return this.http.post<ImportResultResource>(url, formData).toPromise();
    }


    getCustomStore(filter?: {
        stockControlCategoryId?: string,
        itemId?: string
    }) {
        return new CustomStore({
            key: 'id',
            load: (loadOptions) => {
                const skip = loadOptions.skip;
                const size = loadOptions.take;
                let page = Math.floor(skip / size) + 1;
                if (isNaN(page) || page == null) page = 0;

                const params = {
                    stockControlCategoryId: filter?.stockControlCategoryId,
                    itemId: filter?.itemId,
                    page: page, size: size
                };

                return new Promise((resolve, reject) => {
                    this.getList(params)
                        .then(result => {
                            resolve({
                                data: result.data,
                                totalCount: result.totalData
                            });
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            },
            byKey: (key) => {
                return this.get(key);
            },
            insert: (values) => {
                return new Promise((resolve, reject) => {
                    return this.create(values)
                        .then(result => {
                            notify('Data saved successfully.', 'success', 2000);
                            resolve(result);
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });

            },
            update: (key, values) => {
                return new Promise((resolve, reject) => {
                    this.patchUpdate(key, values)
                        .then(result => {
                            notify('Data saved successfully.', 'success', 2000);
                            resolve(result);
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            },
            remove: (key) => {
                return new Promise((resolve, reject) => {
                    this.delete(key)
                        .then(() => {
                            notify('Data deleted successfully.', 'success', 2000);
                            resolve();
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            }
        });
    }
}
