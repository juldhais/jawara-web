import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Helper } from '../helpers/helper';
import { IdentityResource } from '../resources/identity.resource';
import { UserService } from './user.service';

const defaultPath = '/';

@Injectable()
export class AuthService {
    constructor(private router: Router,
        private userService: UserService) { }

    get loggedIn(): boolean {
        const identity = Helper.getIdentity();
        if (!identity) return false;

        const isTokenExpired = this.tokenExpired(identity.token);
        if (isTokenExpired) return false;

        return true;
    }

    private tokenExpired(token: string) {
        if (!token) return true;

        const expiry = (JSON.parse(atob(token.split('.')[1]))).exp;
        return (Math.floor((new Date).getTime() / 1000)) >= expiry;
    }

    private _lastAuthenticatedPath: string = defaultPath;
    set lastAuthenticatedPath(value: string) {
        this._lastAuthenticatedPath = value;
    }

    async login(userName: string, password: string) {
        const identity = await this.userService.login(userName, password);
        Helper.setIdentity(identity);
        this.router.navigate([this._lastAuthenticatedPath]);
    }

    async logOut() {
        localStorage.clear();
        this.router.navigate(['/login']);
    }
}

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(private router: Router, private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot): boolean {
        const isLoggedIn = this.authService.loggedIn;
        const isAuthForm = [
            'login'
        ].includes(route.routeConfig.path);

        if (isLoggedIn && isAuthForm) {
            this.authService.lastAuthenticatedPath = defaultPath;
            this.router.navigate([defaultPath]);
            return false;
        }

        if (!isLoggedIn && !isAuthForm) {
            this.router.navigate(['/login']);
        }

        if (isLoggedIn) {
            this.authService.lastAuthenticatedPath = route.routeConfig.path;
        }

        return isLoggedIn || isAuthForm;
    }
}
