import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TaxResource } from '../resources/tax.resource';
import { PagingResult } from '../resources/paging-result.resource';
import { Helper } from '../helpers/helper';
import { ScalarResource } from '../resources/scalar.resource';
import CustomStore from 'devextreme/data/custom_store';
import notify from 'devextreme/ui/notify';
import { LookupResource } from '../resources/lookup.resource';

@Injectable({
    providedIn: 'root'
})
export class TaxService {

    constructor(private http: HttpClient) { }

    get(id) {
        const url = `tax/${id}`;
        return this.http.get<TaxResource>(url).toPromise();
    }

    getList(params?: {
        keyword?: string, 
        type?: string,
        page?: number, 
        size?: number
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `tax`;
        return this.http.get<PagingResult<TaxResource>>(url, { params: httpParams }).toPromise();
    }

    getSingleLookup(id) {
        const url = `tax/lookup/${id}`;
        return this.http.get<LookupResource>(url).toPromise();
    }

    getLookup(params?: { 
        keyword?: string,
        type?: string
    }) {
        let httpParams = Helper.createHttpParams(params);
        const url = `tax/lookup`;
        return this.http.get<LookupResource[]>(url, { params: httpParams }).toPromise();
    }

    create(resource: TaxResource) {
        const url = `tax`;
        return this.http.post<ScalarResource>(url, resource).toPromise();
    }

    update(resource: TaxResource) {
        const url = `tax/${resource.id}`;
        return this.http.put<ScalarResource>(url, resource).toPromise();
    }

    patchUpdate(id: string, resource: any) {
        const url = `tax/${id}`;
        return this.http.patch<ScalarResource>(url, resource).toPromise();;
    }

    createOrUpdate(resource: TaxResource, mode: string) {
        if (mode == 'create')
            return this.create(resource);
        else if (mode == 'update')
            return this.update(resource);
    }

    delete(id) {
        const url = `tax/${id}`
        return this.http.delete(url).toPromise();
    }

    
    getCustomStore() {
        return new CustomStore({
            key: 'id',
            load: (loadOptions) => {
                const skip = loadOptions.skip;
                const size = loadOptions.take;
                let page = Math.floor(skip / size) + 1;
                if (isNaN(page) || page == null) page = 0;

                let keyword = '';
                if (loadOptions.searchValue)
                    keyword = loadOptions.searchValue;
                else if (loadOptions.filter)
                    keyword = loadOptions.filter[0].filterValue;

                return new Promise((resolve, reject) => {
                    this.getList({ keyword: keyword, page: page, size: size})
                        .then(result => {
                            resolve({
                                data: result.data,
                                totalCount: result.totalData
                            });
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            },
            byKey: (key) => {
                return this.get(key);
            },
            insert: (values) => {
                return new Promise((resolve, reject) => {
                    return this.create(values)
                        .then(result => {
                            notify('Data saved successfully.', 'success', 2000);
                            resolve(result);
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });

            },
            update: (key, values) => {
                return new Promise((resolve, reject) => {
                    this.patchUpdate(key, values)
                        .then(result => {
                            notify('Data saved successfully.', 'success', 2000);
                            resolve(result);
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            },
            remove: (key) => {
                return new Promise((resolve, reject) => {
                    this.delete(key)
                        .then(() => {
                            notify('Data deleted successfully.', 'success', 2000);
                            resolve();
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            }
        });
    }

    getLookupCustomStore() {
        return new CustomStore({
            key: 'id',
            load: (loadOptions) => {
                const keyword = loadOptions.searchValue;
                    if (!keyword || keyword.length < 2) return;

                const params = { keyword: keyword };

                return new Promise((resolve, reject) => {
                    this.getLookup(params)
                        .then(result => {
                            resolve({
                                data: result,
                                totalCount: result.length
                            });
                        }, error => {
                            if ('error' in error) reject(error.error.message);
                            else reject(error.message);
                        });
                });
            },
            byKey: (key) => {
                return this.getSingleLookup(key);
            }
        });
    }
}
