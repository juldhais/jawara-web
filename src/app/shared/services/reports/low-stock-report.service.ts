import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LowStockReportResource, LowStockReportParam } from '../../resources/reports/low-stock-report.resource';

@Injectable({
    providedIn: 'root'
})
export class LowStockReportService {

    constructor(private http: HttpClient) { }

    getReport(param: LowStockReportParam) {
        const url = `low-stock-report`;
        return this.http.post<LowStockReportResource>(url, param).toPromise();
    }
}
