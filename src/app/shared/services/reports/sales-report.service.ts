import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SalesReportResource, SalesReportParam } from '../../resources/reports/sales-report.resource';

@Injectable({
    providedIn: 'root'
})
export class SalesReportService {

    constructor(private http: HttpClient) { }

    getReport(param: SalesReportParam) {
        const url = `sales-report`;
        return this.http.post<SalesReportResource>(url, param).toPromise();
    }
}
