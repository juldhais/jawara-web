import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PurchaseDetailReportParam, PurchaseDetailReportResource } from '../../resources/reports/purchase-detail-report.resource';

@Injectable({
    providedIn: 'root'
})
export class PurchaseDetailReportService {

    constructor(private http: HttpClient) { }

    getReport(param: PurchaseDetailReportParam) {
        const url = `purchase-detail-report`;
        return this.http.post<PurchaseDetailReportResource>(url, param).toPromise();
    }
}
