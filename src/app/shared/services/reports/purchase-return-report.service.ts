import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PurchaseReturnReportParam, PurchaseReturnReportResource } from '../../resources/reports/purchase-return-report.resource';

@Injectable({
    providedIn: 'root'
})
export class PurchaseReturnReportService {

    constructor(private http: HttpClient) { }

    getReport(param: PurchaseReturnReportParam) {
        const url = `purchase-return-report`;
        return this.http.post<PurchaseReturnReportResource>(url, param).toPromise();
    }
}
