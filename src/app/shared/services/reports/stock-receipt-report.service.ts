import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StockReceiptReportResource, StockReceiptReportParam } from '../../resources/reports/stock-receipt-report.resource';

@Injectable({
    providedIn: 'root'
})
export class StockReceiptReportService {

    constructor(private http: HttpClient) { }

    getReport(param: StockReceiptReportParam) {
        const url = `stock-receipt-report`;
        return this.http.post<StockReceiptReportResource>(url, param).toPromise();
    }
}
