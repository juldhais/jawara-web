import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StockDeliveryReportResource, StockDeliveryReportParam } from '../../resources/reports/stock-delivery-report.resource';

@Injectable({
    providedIn: 'root'
})
export class StockDeliveryReportService {

    constructor(private http: HttpClient) { }

    getReport(param: StockDeliveryReportParam) {
        const url = `stock-delivery-report`;
        return this.http.post<StockDeliveryReportResource>(url, param).toPromise();
    }
}
