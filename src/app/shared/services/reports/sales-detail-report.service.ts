import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SalesDetailReportParam, SalesDetailReportResource } from '../../resources/reports/sales-detail-report.resource';

@Injectable({
    providedIn: 'root'
})
export class SalesDetailReportService {

    constructor(private http: HttpClient) { }

    getReport(param: SalesDetailReportParam) {
        const url = `sales-detail-report`;
        return this.http.post<SalesDetailReportResource>(url, param).toPromise();
    }
}
