import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StockMovementReportParam, StockMovementReportResource } from '../../resources/reports/stock-movement-report.resource';

@Injectable({
    providedIn: 'root'
})
export class StockMovementReportService {

    constructor(private http: HttpClient) { }

    getReport(param: StockMovementReportParam) {
        const url = `stock-movement-report`;
        return this.http.post<StockMovementReportResource>(url, param).toPromise();
    }
}
