import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PurchaseReportResource, PurchaseReportParam } from '../../resources/reports/purchase-report.resource';

@Injectable({
    providedIn: 'root'
})
export class PurchaseReportService {

    constructor(private http: HttpClient) { }

    getReport(param: PurchaseReportParam) {
        const url = `purchase-report`;
        return this.http.post<PurchaseReportResource>(url, param).toPromise();
    }
}
