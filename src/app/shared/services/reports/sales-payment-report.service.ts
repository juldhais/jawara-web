import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SalesPaymentReportResource, SalesPaymentReportParam } from '../../resources/reports/sales-payment-report.resource';

@Injectable({
    providedIn: 'root'
})
export class SalesPaymentReportService {

    constructor(private http: HttpClient) { }

    getReport(param: SalesPaymentReportParam) {
        const url = `sales-payment-report`;
        return this.http.post<SalesPaymentReportResource>(url, param).toPromise();
    }
}
