import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SalesReturnReportParam, SalesReturnReportResource } from '../../resources/reports/sales-return-report.resource';

@Injectable({
    providedIn: 'root'
})
export class SalesReturnReportService {

    constructor(private http: HttpClient) { }

    getReport(param: SalesReturnReportParam) {
        const url = `sales-return-report`;
        return this.http.post<SalesReturnReportResource>(url, param).toPromise();
    }
}
