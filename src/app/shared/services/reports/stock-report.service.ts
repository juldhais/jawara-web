import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StockReportResource, StockReportParam } from '../../resources/reports/stock-report.resource';

@Injectable({
    providedIn: 'root'
})
export class StockReportService {

    constructor(private http: HttpClient) { }

    getReport(param: StockReportParam) {
        const url = `stock-report`;
        return this.http.post<StockReportResource>(url, param).toPromise();
    }
}
