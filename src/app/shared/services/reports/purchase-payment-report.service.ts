import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PurchasePaymentReportResource, PurchasePaymentReportParam } from '../../resources/reports/purchase-payment-report.resource';

@Injectable({
    providedIn: 'root'
})
export class PurchasePaymentReportService {

    constructor(private http: HttpClient) { }

    getReport(param: PurchasePaymentReportParam) {
        const url = `purchase-payment-report`;
        return this.http.post<PurchasePaymentReportResource>(url, param).toPromise();
    }
}
