import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


export interface ErrorInfo {
    status: number,
    message: string
}

@Injectable({
    providedIn: 'root'
})
export class ErrorHelper {
    constructor(private router: Router) { }

    getErrorInfo(error): ErrorInfo {
        if (error instanceof HttpErrorResponse) {
            if (error.status == 0) {
                return {
                    status: 0,
                    message: 'Cannot connect to the server.'
                }
            }

            if (error.status == 401) {
                return {
                    status: 401,
                    message: 'Please login to proceed.'
                }
            }

            if (error.error && error.error.message) {
                return {
                    status: error.error.statusCode,
                    message: error.error.message
                }
            }
        }

        return {
            status: error.status,
            message: error.message
        }
    }

    handleError(error) {
        const errorInfo = this.getErrorInfo(error);
        
        if (errorInfo.status == 401) {
            this.router.navigateByUrl('/login');
        }

        return errorInfo;
    }
}