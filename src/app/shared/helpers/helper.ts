import { HttpParams } from '@angular/common/http';
import { IdentityResource } from '../resources/identity.resource';

export const Helper = {
    copyObject: (target, source) => {
        for (let key in source) {
            target[key] = source[key];
        }
    },

    createHttpParams: (params) => {
        let httpParams = new HttpParams();
        
        for (let key in params)
            if (params[key]) httpParams = httpParams.append(key, params[key]);
        
        return httpParams;
    },

    dateForHttpParam: (date: Date) => {
        if (date) {
            console.log(date);
            const day = ("0" + date.getDate()).slice(-2);
            const month = ("0" + (date.getMonth() + 1)).slice(-2);
            const year = date.getFullYear();

            return `${year}-${month}-${day}`;
        }

        return '';
    },

    dateTimeForHttpParam: (date: Date) => {
        if (date) {
            const day = ("0" + date.getDate()).slice(-2);
            const month = ("0" + (date.getMonth() + 1)).slice(-2);
            const year = date.getFullYear();
            const hour = ("0" + date.getHours()).slice(-2);
            const minute = ("0" + date.getMinutes()).slice(-2);

            return `${year}-${month}-${day}T${hour}:${minute}`;
        }

        return '';
    },

    cleanString: (value: string) => {
        if (value == null || value === undefined) return '';

        return value;
    },

    isStringEmpty: (value: string) => {
        return value == null || value === undefined || value == '';
    },

    isStringNotEmpty: (value: string) => {
        return !Helper.isStringEmpty(value);
    },

    encrypt: (text: string) => {
        if (Helper.isStringEmpty(text)) return '';

        const first = btoa(text);
        const reverse = first.split('').reverse().join('');
        const second = btoa(reverse);
        return second;
    },

    decrypt: (text: string) => {
        if (Helper.isStringEmpty(text)) return '';

        const reverse = atob(text);
        const first = reverse.split('').reverse().join('');
        const original = atob(first);
        return original;
    },

    setLocalStorageItem: (key: string, value: any) => {
        const encryptedKey = Helper.encrypt(key);
        const encryptedValue = Helper.encrypt(JSON.stringify(value));

        localStorage.setItem(encryptedKey, encryptedValue);
    },

    getLocalStorageItem: (key: string) => {
        const encryptedKey = Helper.encrypt(key);
        const encryptedValue = localStorage.getItem(encryptedKey);
        const decryptedValue = Helper.decrypt(encryptedValue);

        if (Helper.isStringEmpty(decryptedValue)) return null;

        const value = JSON.parse(decryptedValue);

        return value;
    },

    removeLocalStorageItem: (key: string) => {
        const encryptedKey = Helper.encrypt(key);
        localStorage.removeItem(encryptedKey);
    },

    setIdentity: (identity: IdentityResource) => {
        Helper.setLocalStorageItem('identity', identity);
    },

    getIdentity: () => {
        const identity: IdentityResource = Helper.getLocalStorageItem('identity');
        return identity;
    },

    getUserPrivilege() {
        const identity = Helper.getIdentity();
        if (!identity) return null;

        const result: UserPrivilege = {
            isAdministrator: identity.role.isAdministrator,

            item: identity.role.privileges.includes('Item') || identity.role.isAdministrator,
            itemCreate: identity.role.privileges.includes('Item - Create') || identity.role.isAdministrator,
            itemUpdate: identity.role.privileges.includes('Item - Update') || identity.role.isAdministrator,
            itemDelete: identity.role.privileges.includes('Item - Delete') || identity.role.isAdministrator,
            
            itemCategory: identity.role.privileges.includes('Item Category') || identity.role.isAdministrator,
            itemCategoryCreate: identity.role.privileges.includes('Item Category - Create') || identity.role.isAdministrator,
            itemCategoryUpdate: identity.role.privileges.includes('Item Category - Update') || identity.role.isAdministrator,
            itemCategoryDelete: identity.role.privileges.includes('Item Category - Delete') || identity.role.isAdministrator,
            
            itemSubcategory: identity.role.privileges.includes('Item Subcategory') || identity.role.isAdministrator,
            itemSubcategoryCreate: identity.role.privileges.includes('Item Subcategory - Create') || identity.role.isAdministrator,
            itemSubcategoryUpdate: identity.role.privileges.includes('Item Subcategory - Update') || identity.role.isAdministrator,
            itemSubcategoryDelete: identity.role.privileges.includes('Item Subcategory - Delete') || identity.role.isAdministrator,
            
            itemTag: identity.role.privileges.includes('Item Tag') || identity.role.isAdministrator,
            itemTagCreate: identity.role.privileges.includes('Item Tag - Create') || identity.role.isAdministrator,
            itemTagUpdate: identity.role.privileges.includes('Item Tag - Update') || identity.role.isAdministrator,
            itemTagDelete: identity.role.privileges.includes('Item Tag - Delete') || identity.role.isAdministrator,
            
            priceControl: identity.role.privileges.includes('Price Control') || identity.role.isAdministrator,
            priceControlCreate: identity.role.privileges.includes('Price Control - Create') || identity.role.isAdministrator,
            priceControlUpdate: identity.role.privileges.includes('Price Control - Update') || identity.role.isAdministrator,
            priceControlDelete: identity.role.privileges.includes('Price Control - Delete') || identity.role.isAdministrator,
            
            stockControl: identity.role.privileges.includes('Stock Control') || identity.role.isAdministrator,
            stockControlCreate: identity.role.privileges.includes('Stock Control - Create') || identity.role.isAdministrator,
            stockControlUpdate: identity.role.privileges.includes('Stock Control - Update') || identity.role.isAdministrator,
            stockControlDelete: identity.role.privileges.includes('Stock Control - Delete') || identity.role.isAdministrator,
            
            billOfMaterial: identity.role.privileges.includes('Bill of Material') || identity.role.isAdministrator,
            billOfMaterialCreate: identity.role.privileges.includes('Bill of Material - Create') || identity.role.isAdministrator,
            billOfMaterialUpdate: identity.role.privileges.includes('Bill of Material - Update') || identity.role.isAdministrator,
            billOfMaterialDelete: identity.role.privileges.includes('Bill of Material - Delete') || identity.role.isAdministrator,
            
            unit: identity.role.privileges.includes('Unit') || identity.role.isAdministrator,
            unitCreate: identity.role.privileges.includes('Unit - Create') || identity.role.isAdministrator,
            unitUpdate: identity.role.privileges.includes('Unit - Update') || identity.role.isAdministrator,
            unitDelete: identity.role.privileges.includes('Unit - Delete') || identity.role.isAdministrator,
            
            location: identity.role.privileges.includes('Location') || identity.role.isAdministrator,
            locationCreate: identity.role.privileges.includes('Location - Create') || identity.role.isAdministrator,
            locationUpdate: identity.role.privileges.includes('Location - Update') || identity.role.isAdministrator,
            locationDelete: identity.role.privileges.includes('Location - Delete') || identity.role.isAdministrator,
            
            priceControlCategory: identity.role.privileges.includes('Price Control Category') || identity.role.isAdministrator,
            priceControlCategoryCreate: identity.role.privileges.includes('Price Control Category - Create') || identity.role.isAdministrator,
            priceControlCategoryUpdate: identity.role.privileges.includes('Price Control Category - Update') || identity.role.isAdministrator,
            priceControlCategoryDelete: identity.role.privileges.includes('Price Control Category - Delete') || identity.role.isAdministrator,
            
            stockControlCategory: identity.role.privileges.includes('Stock Control Category') || identity.role.isAdministrator,
            stockControlCategoryCreate: identity.role.privileges.includes('Stock Control Category - Create') || identity.role.isAdministrator,
            stockControlCategoryUpdate: identity.role.privileges.includes('Stock Control Category - Update') || identity.role.isAdministrator,
            stockControlCategoryDelete: identity.role.privileges.includes('Stock Control Category - Delete') || identity.role.isAdministrator,
            
            customer: identity.role.privileges.includes('Customer') || identity.role.isAdministrator,
            customerCreate: identity.role.privileges.includes('Customer - Create') || identity.role.isAdministrator,
            customerUpdate: identity.role.privileges.includes('Customer - Update') || identity.role.isAdministrator,
            customerDelete: identity.role.privileges.includes('Customer - Delete') || identity.role.isAdministrator,
            
            customerCategory: identity.role.privileges.includes('Customer Category') || identity.role.isAdministrator,
            customerCategoryCreate: identity.role.privileges.includes('Customer Category - Create') || identity.role.isAdministrator,
            customerCategoryUpdate: identity.role.privileges.includes('Customer Category - Update') || identity.role.isAdministrator,
            customerCategoryDelete: identity.role.privileges.includes('Customer Category - Delete') || identity.role.isAdministrator,
            
            supplier: identity.role.privileges.includes('Supplier') || identity.role.isAdministrator,
            supplierCreate: identity.role.privileges.includes('Supplier - Create') || identity.role.isAdministrator,
            supplierUpdate: identity.role.privileges.includes('Supplier - Update') || identity.role.isAdministrator,
            supplierDelete: identity.role.privileges.includes('Supplier - Delete') || identity.role.isAdministrator,
            
            supplierCategory: identity.role.privileges.includes('Supplier Category') || identity.role.isAdministrator,
            supplierCategoryCreate: identity.role.privileges.includes('Supplier Category - Create') || identity.role.isAdministrator,
            supplierCategoryUpdate: identity.role.privileges.includes('Supplier Category - Update') || identity.role.isAdministrator,
            supplierCategoryDelete: identity.role.privileges.includes('Supplier Category - Delete') || identity.role.isAdministrator,
            
            paymentMethod: identity.role.privileges.includes('Payment Method') || identity.role.isAdministrator,
            paymentMethodCreate: identity.role.privileges.includes('Payment Method - Create') || identity.role.isAdministrator,
            paymentMethodUpdate: identity.role.privileges.includes('Payment Method - Update') || identity.role.isAdministrator,
            paymentMethodDelete: identity.role.privileges.includes('Payment Method - Delete') || identity.role.isAdministrator,
            
            role: identity.role.privileges.includes('Role') || identity.role.isAdministrator,
            roleCreate: identity.role.privileges.includes('Role - Create') || identity.role.isAdministrator,
            roleUpdate: identity.role.privileges.includes('Role - Update') || identity.role.isAdministrator,
            roleDelete: identity.role.privileges.includes('Role - Delete') || identity.role.isAdministrator,
            
            user: identity.role.privileges.includes('User') || identity.role.isAdministrator,
            userCreate: identity.role.privileges.includes('User - Create') || identity.role.isAdministrator,
            userUpdate: identity.role.privileges.includes('User - Update') || identity.role.isAdministrator,
            userDelete: identity.role.privileges.includes('User - Delete') || identity.role.isAdministrator,
            
            configuration: identity.role.privileges.includes('Configuration') || identity.role.isAdministrator,
            
            purchaseOrder: identity.role.privileges.includes('Purchase Order') || identity.role.isAdministrator,
            purchaseOrderCreate: identity.role.privileges.includes('Purchase Order - Create') || identity.role.isAdministrator,
            purchaseOrderUpdate: identity.role.privileges.includes('Purchase Order - Update') || identity.role.isAdministrator,
            purchaseOrderDelete: identity.role.privileges.includes('Purchase Order - Delete') || identity.role.isAdministrator,
            purchaseOrderCancel: identity.role.privileges.includes('Purchase Order - Cancel') || identity.role.isAdministrator,
            purchaseOrderOpen: identity.role.privileges.includes('Purchase Order - Open') || identity.role.isAdministrator,
            
            purchaseReceipt: identity.role.privileges.includes('Purchase Receipt') || identity.role.isAdministrator,
            purchaseReceiptCreate: identity.role.privileges.includes('Purchase Receipt - Create') || identity.role.isAdministrator,
            purchaseReceiptUpdate: identity.role.privileges.includes('Purchase Receipt - Update') || identity.role.isAdministrator,
            purchaseReceiptDelete: identity.role.privileges.includes('Purchase Receipt - Delete') || identity.role.isAdministrator,
            
            purchaseReturn: identity.role.privileges.includes('Purchase Return') || identity.role.isAdministrator,
            purchaseReturnCreate: identity.role.privileges.includes('Purchase Return - Create') || identity.role.isAdministrator,
            purchaseReturnUpdate: identity.role.privileges.includes('Purchase Return - Update') || identity.role.isAdministrator,
            purchaseReturnDelete: identity.role.privileges.includes('Purchase Return - Delete') || identity.role.isAdministrator,
            
            purchasePayment: identity.role.privileges.includes('Purchase Payment') || identity.role.isAdministrator,
            purchasePaymentCreate: identity.role.privileges.includes('Purchase Payment - Create') || identity.role.isAdministrator,
            purchasePaymentUpdate: identity.role.privileges.includes('Purchase Payment - Update') || identity.role.isAdministrator,
            purchasePaymentDelete: identity.role.privileges.includes('Purchase Payment - Delete') || identity.role.isAdministrator,
            
            salesOrder: identity.role.privileges.includes('Sales Order') || identity.role.isAdministrator,
            salesOrderCreate: identity.role.privileges.includes('Sales Order - Create') || identity.role.isAdministrator,
            salesOrderUpdate: identity.role.privileges.includes('Sales Order - Update') || identity.role.isAdministrator,
            salesOrderDelete: identity.role.privileges.includes('Sales Order - Delete') || identity.role.isAdministrator,
            salesOrderCancel: identity.role.privileges.includes('Sales Order - Cancel') || identity.role.isAdministrator,
            salesOrderOpen: identity.role.privileges.includes('Sales Order - Open') || identity.role.isAdministrator,
            
            salesDelivery: identity.role.privileges.includes('Sales Delivery') || identity.role.isAdministrator,
            salesDeliveryCreate: identity.role.privileges.includes('Sales Delivery - Create') || identity.role.isAdministrator,
            salesDeliveryUpdate: identity.role.privileges.includes('Sales Delivery - Update') || identity.role.isAdministrator,
            salesDeliveryDelete: identity.role.privileges.includes('Sales Delivery - Delete') || identity.role.isAdministrator,
            
            salesReturn: identity.role.privileges.includes('Sales Return') || identity.role.isAdministrator,
            salesReturnCreate: identity.role.privileges.includes('Sales Return - Create') || identity.role.isAdministrator,
            salesReturnUpdate: identity.role.privileges.includes('Sales Return - Update') || identity.role.isAdministrator,
            salesReturnDelete: identity.role.privileges.includes('Sales Return - Delete') || identity.role.isAdministrator,
            
            salesPayment: identity.role.privileges.includes('Sales Payment') || identity.role.isAdministrator,
            salesPaymentCreate: identity.role.privileges.includes('Sales Payment - Create') || identity.role.isAdministrator,
            salesPaymentUpdate: identity.role.privileges.includes('Sales Payment - Update') || identity.role.isAdministrator,
            salesPaymentDelete: identity.role.privileges.includes('Sales Payment - Delete') || identity.role.isAdministrator,
            
            stockList: identity.role.privileges.includes('Stock List') || identity.role.isAdministrator,
            lowStockList: identity.role.privileges.includes('Low Stock List') || identity.role.isAdministrator,
            
            manufacturingOrder: identity.role.privileges.includes('Manufacturing Order') || identity.role.isAdministrator,
            manufacturingOrderCreate: identity.role.privileges.includes('Manufacturing Order - Create') || identity.role.isAdministrator,
            manufacturingOrderUpdate: identity.role.privileges.includes('Manufacturing Order - Update') || identity.role.isAdministrator,
            manufacturingOrderDelete: identity.role.privileges.includes('Manufacturing Order - Delete') || identity.role.isAdministrator,
            
            stockOrder: identity.role.privileges.includes('Stock Order') || identity.role.isAdministrator,
            stockOrderCreate: identity.role.privileges.includes('Stock Order - Create') || identity.role.isAdministrator,
            stockOrderUpdate: identity.role.privileges.includes('Stock Order - Update') || identity.role.isAdministrator,
            stockOrderDelete: identity.role.privileges.includes('Stock Order - Delete') || identity.role.isAdministrator,
            stockOrderCancel: identity.role.privileges.includes('Stock Order - Cancel') || identity.role.isAdministrator,
            stockOrderOpen: identity.role.privileges.includes('Stock Order - Open') || identity.role.isAdministrator,
            
            stockDelivery: identity.role.privileges.includes('Stock Delivery') || identity.role.isAdministrator,
            stockDeliveryCreate: identity.role.privileges.includes('Stock Delivery - Create') || identity.role.isAdministrator,
            stockDeliveryUpdate: identity.role.privileges.includes('Stock Delivery - Update') || identity.role.isAdministrator,
            stockDeliveryDelete: identity.role.privileges.includes('Stock Delivery - Delete') || identity.role.isAdministrator,
            
            stockReceipt: identity.role.privileges.includes('Stock Receipt') || identity.role.isAdministrator,
            stockReceiptCreate: identity.role.privileges.includes('Stock Receipt - Create') || identity.role.isAdministrator,
            stockReceiptUpdate: identity.role.privileges.includes('Stock Receipt - Update') || identity.role.isAdministrator,
            stockReceiptDelete: identity.role.privileges.includes('Stock Receipt - Delete') || identity.role.isAdministrator,
            
            stockAdjustment: identity.role.privileges.includes('Stock Adjustment') || identity.role.isAdministrator,
            stockAdjustmentCreate: identity.role.privileges.includes('Stock Adjustment - Create') || identity.role.isAdministrator,
            stockAdjustmentUpdate: identity.role.privileges.includes('Stock Adjustment - Update') || identity.role.isAdministrator,
            stockAdjustmentDelete: identity.role.privileges.includes('Stock Adjustment - Delete') || identity.role.isAdministrator,
            
            cashIn: identity.role.privileges.includes('Cash In') || identity.role.isAdministrator,
            cashInCreate: identity.role.privileges.includes('Cash In - Create') || identity.role.isAdministrator,
            cashInUpdate: identity.role.privileges.includes('Cash In - Update') || identity.role.isAdministrator,
            cashInDelete: identity.role.privileges.includes('Cash In - Delete') || identity.role.isAdministrator,
            
            cashOut: identity.role.privileges.includes('Cash Out') || identity.role.isAdministrator,
            cashOutCreate: identity.role.privileges.includes('Cash Out - Create') || identity.role.isAdministrator,
            cashOutUpdate: identity.role.privileges.includes('Cash Out - Update') || identity.role.isAdministrator,
            cashOutDelete: identity.role.privileges.includes('Cash Out - Delete') || identity.role.isAdministrator,
            
            settlement: identity.role.privileges.includes('Settlement') || identity.role.isAdministrator,
            settlementCreate: identity.role.privileges.includes('Settlement - Create') || identity.role.isAdministrator,
            settlementUpdate: identity.role.privileges.includes('Settlement - Update') || identity.role.isAdministrator,
            settlementDelete: identity.role.privileges.includes('Settlement - Delete') || identity.role.isAdministrator,
            
            changeTransactionDate: identity.role.privileges.includes('Change Transaction Date') || identity.role.isAdministrator,
            changeTransactionLocation: identity.role.privileges.includes('Change Transaction Location') || identity.role.isAdministrator,
            changePurchasePrice: identity.role.privileges.includes('Change Purchase Price') || identity.role.isAdministrator,
            changeSalesPrice: identity.role.privileges.includes('Change Sales Price') || identity.role.isAdministrator,
            
            salesReport: identity.role.privileges.includes('Sales Report') || identity.role.isAdministrator,
            salesProfitReport: identity.role.privileges.includes('Sales Profit Report') || identity.role.isAdministrator,
            purchaseReport: identity.role.privileges.includes('Purchase Report') || identity.role.isAdministrator,
            stockReport: identity.role.privileges.includes('Stock Report') || identity.role.isAdministrator,
        };

        return result;
    },

    printHtml(html) {
        const printWindow = window.open();
        printWindow.document.write(html);
        printWindow.focus();
        printWindow.stop();
        printWindow.print();
    }
};

export class UserPrivilege {
    isAdministrator: boolean;

    item: boolean;
    itemCreate: boolean;
    itemUpdate: boolean;
    itemDelete: boolean;

    itemCategory: boolean;
    itemCategoryCreate: boolean;
    itemCategoryUpdate: boolean;
    itemCategoryDelete: boolean;

    itemSubcategory: boolean;
    itemSubcategoryCreate: boolean;
    itemSubcategoryUpdate: boolean;
    itemSubcategoryDelete: boolean;

    itemTag: boolean;
    itemTagCreate: boolean;
    itemTagUpdate: boolean;
    itemTagDelete: boolean;

    priceControl: boolean;
    priceControlCreate: boolean;
    priceControlUpdate: boolean;
    priceControlDelete: boolean;

    stockControl: boolean;
    stockControlCreate: boolean;
    stockControlUpdate: boolean;
    stockControlDelete: boolean;

    billOfMaterial: boolean;
    billOfMaterialCreate: boolean;
    billOfMaterialUpdate: boolean;
    billOfMaterialDelete: boolean;

    unit: boolean;
    unitCreate: boolean;
    unitUpdate: boolean;
    unitDelete: boolean;

    location: boolean;
    locationCreate: boolean;
    locationUpdate: boolean;
    locationDelete: boolean;

    priceControlCategory: boolean;
    priceControlCategoryCreate: boolean;
    priceControlCategoryUpdate: boolean;
    priceControlCategoryDelete: boolean;

    stockControlCategory: boolean;
    stockControlCategoryCreate: boolean;
    stockControlCategoryUpdate: boolean;
    stockControlCategoryDelete: boolean;

    customer: boolean;
    customerCreate: boolean;
    customerUpdate: boolean;
    customerDelete: boolean;

    customerCategory: boolean;
    customerCategoryCreate: boolean;
    customerCategoryUpdate: boolean;
    customerCategoryDelete: boolean;

    supplier: boolean;
    supplierCreate: boolean;
    supplierUpdate: boolean;
    supplierDelete: boolean;

    supplierCategory: boolean;
    supplierCategoryCreate: boolean;
    supplierCategoryUpdate: boolean;
    supplierCategoryDelete: boolean;

    paymentMethod: boolean;
    paymentMethodCreate: boolean;
    paymentMethodUpdate: boolean;
    paymentMethodDelete: boolean;

    role: boolean;
    roleCreate: boolean;
    roleUpdate: boolean;
    roleDelete: boolean;

    user: boolean;
    userCreate: boolean;
    userUpdate: boolean;
    userDelete: boolean;

    configuration: boolean;

    purchaseOrder: boolean;
    purchaseOrderCreate: boolean;
    purchaseOrderUpdate: boolean;
    purchaseOrderDelete: boolean;
    purchaseOrderCancel: boolean;
    purchaseOrderOpen: boolean;

    purchaseReceipt: boolean;
    purchaseReceiptCreate: boolean;
    purchaseReceiptUpdate: boolean;
    purchaseReceiptDelete: boolean;

    purchaseReturn: boolean;
    purchaseReturnCreate: boolean;
    purchaseReturnUpdate: boolean;
    purchaseReturnDelete: boolean;

    purchasePayment: boolean;
    purchasePaymentCreate: boolean;
    purchasePaymentUpdate: boolean;
    purchasePaymentDelete: boolean;

    salesOrder: boolean;
    salesOrderCreate: boolean;
    salesOrderUpdate: boolean;
    salesOrderDelete: boolean;
    salesOrderCancel: boolean;
    salesOrderOpen: boolean;

    salesDelivery: boolean;
    salesDeliveryCreate: boolean;
    salesDeliveryUpdate: boolean;
    salesDeliveryDelete: boolean;

    salesReturn: boolean;
    salesReturnCreate: boolean;
    salesReturnUpdate: boolean;
    salesReturnDelete: boolean;

    salesPayment: boolean;
    salesPaymentCreate: boolean;
    salesPaymentUpdate: boolean;
    salesPaymentDelete: boolean;

    stockList: boolean;
    lowStockList: boolean;

    manufacturingOrder: boolean;
    manufacturingOrderCreate: boolean;
    manufacturingOrderUpdate: boolean;
    manufacturingOrderDelete: boolean;

    stockOrder: boolean;
    stockOrderCreate: boolean;
    stockOrderUpdate: boolean;
    stockOrderDelete: boolean;
    stockOrderCancel: boolean;
    stockOrderOpen: boolean;

    stockDelivery: boolean;
    stockDeliveryCreate: boolean;
    stockDeliveryUpdate: boolean;
    stockDeliveryDelete: boolean;

    stockReceipt: boolean;
    stockReceiptCreate: boolean;
    stockReceiptUpdate: boolean;
    stockReceiptDelete: boolean;

    stockAdjustment: boolean;
    stockAdjustmentCreate: boolean;
    stockAdjustmentUpdate: boolean;
    stockAdjustmentDelete: boolean;

    cashIn: boolean;
    cashInCreate: boolean;
    cashInUpdate: boolean;
    cashInDelete: boolean;

    cashOut: boolean;
    cashOutCreate: boolean;
    cashOutUpdate: boolean;
    cashOutDelete: boolean;

    settlement: boolean;
    settlementCreate: boolean;
    settlementUpdate: boolean;
    settlementDelete: boolean;

    changeTransactionDate: boolean;
    changeTransactionLocation: boolean;
    changeSalesPrice: boolean;
    changePurchasePrice: boolean;

    salesReport: boolean;
    salesProfitReport: boolean;
    purchaseReport: boolean;
    stockReport: boolean;
}