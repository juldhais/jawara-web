export const DomHelper = {

    setDocumentTitle: (title: string) => {
        document.title = title;
    },

    getDocumentTitle: () => {
        return document.title;
    },

    setPageTitle: (title: string) => {
        document.getElementById('page-title').innerText = title;
    },

    getPageTitle: () => {
        return document.getElementById('page-title').innerText;
    },

    setText: (id: string, text: string) => {
        document.getElementById(id).innerText = text;
    },

    getText: (id: string) => {
        return document.getElementById(id).innerText;
    },

    setToolbarTitle: (title: string) => {
        document.getElementById('toolbar-title').innerText = title;
    },
}