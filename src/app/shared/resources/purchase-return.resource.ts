export class PurchaseReturnResource {
    id: string;
    documentNumber: string;
    documentDate: Date;
    referenceNumber: string;
    status: string;
    purchaseOrderId: string;
    purchasePaymentId: string;
    locationCode: string;
    locationId: string;
    supplierCode: string;
    supplierDescription: string;
    supplierId: string;
    totalBeforeTax: number = 0;
    totalValueAddedTax: number = 0;
    totalIncomeTax: number = 0;
    totalOtherTax: number = 0;
    totalAfterTax: number = 0;
    remarks: string;
    details: PurchaseReturnDetailResource[] = [];
}

export class PurchaseReturnDetailResource {
    id: string;
    purchaseReturnId: string;
    sequence: number;
    itemCode: string;
    itemDescription: string;
    itemId: string;
    unit: string;
    ratio: number = 0;
    quantity: number = 0;
    price: number = 0;
    discountPercent1: number = 0;
    discountPercent2: number = 0;
    discountAmount: number = 0;
    subtotalBeforeTax: number = 0;
    valueAddedTaxCode: string;
    valueAddedTaxId: string;
    valueAddedTaxAmount: number = 0;
    incomeTaxCode: string;
    incomeTaxId: string;
    incomeTaxAmount: number = 0;
    otherTaxCode: string;
    otherTaxId: string;
    otherTaxAmount: number = 0;
    subtotalAfterTax: number = 0;
    remarks: string;
}