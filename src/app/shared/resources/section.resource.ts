export class SectionResource {
    id: string;
    code: string;
    description: string;
    remarks: string;
}