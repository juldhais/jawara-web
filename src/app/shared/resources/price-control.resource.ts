export class PriceControlResource {
    id: string;
    priceControlCategoryCode: string;
    priceControlCategoryId: string;
    itemCode: string;
    itemDescription: string;
    itemId: string;
    unit: string;
    ratio: number;
    price: number;
    minimumQuantity: number;
}