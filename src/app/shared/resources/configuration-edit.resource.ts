export class ConfigurationEditResource {
    companyName: string;
    printHeader: string;
    printSubheader: string;
    printFooter: string;
    autoCutter: string;
    printType: string;
    useDiscount: string;
    useTax: string;
}