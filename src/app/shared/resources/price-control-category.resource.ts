export class PriceControlCategoryResource {
    id: string;
    code: string;
    description: string;
}