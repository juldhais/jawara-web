export class BatchUpdatePriceControlResource {
    priceControlCategoryId: string ;
    itemCategoryId: string;
    itemSubcategoryId: string;
    itemTag: string ;
    unit: string;
    ratio: number = 1;
    minimumQuantity: number = 0;
    price: number = 0;
}