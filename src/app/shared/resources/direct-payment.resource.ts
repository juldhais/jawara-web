export class DirectPaymentResource {
    paymentId: string;
    paymentMethodCode: string;
    paymentMethodId: string;
    amount: number = 0;
    remarks: string;
}