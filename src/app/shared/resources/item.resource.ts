import { ItemUnitResource } from './item-unit.resource';

export class ItemResource {
    id: string;
    code: string;
    description: string;
    barcode: string;
    unit: string;
    purchasePrice: number;
    salesPrice: number;
    purchasable: boolean;
    stockable: boolean;
    manufacturable: boolean;
    sellable: boolean;
    itemCategoryCode: string;
    itemCategoryId: string;
    itemSubcategoryCode: string;
    itemSubcategoryId: string;
    tag: string;
    supplierCode: string;
    supplierId: string;
    supplierItemCode: string;
    image: string;
    remarks: string;
    units: ItemUnitResource[] = [];
}