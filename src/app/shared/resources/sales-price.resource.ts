export class SalesPriceResource {
    itemId: string;
    locationId: string;
    customerId: string;
    unit: string;
    ratio: number;
    quantity: number;
}