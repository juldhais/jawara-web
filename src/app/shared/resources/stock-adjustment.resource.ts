export class StockAdjustmentResource {
    id: string;
    documentNumber: string;
    documentDate: Date;
    reason: string;
    locationCode: string;
    locationId: string;
    remarks: string;
    details: StockAdjustmentDetailResource[] = [];
}

export class StockAdjustmentDetailResource {
    id: string;
    stockAdjustmentId: string;
    sequence: number;
    itemCode: string;
    itemDescription: string;
    itemId: string;
    unit: string;
    ratio: number = 0;
    quantity: number = 0;
    cost: number = 0;
    subtotal: number = 0;
    remarks: string = '';
}