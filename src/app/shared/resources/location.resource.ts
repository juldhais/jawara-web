export class LocationResource {
    id: string;
    code: string;
    description: string;
    type: string;
    
    priceControlCategoryCode: string;
    priceControlCategoryId: string;
    stockControlCategoryCode: string;
    stockControlCategoryId: string;

    address: string;
    phone: string;
    email: string;
    contact: string;

    remarks: string;
}