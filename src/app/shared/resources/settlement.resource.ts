export class SettlementResource {
    id: string;
    documentNumber: string;
    documentDate: Date;
    locationCode: string;
    locationId: string;
    userName: string;
    userId: string;
    details: SettlementDetailResource[] = [];
}

export class SettlementDetailResource {
    id: string;
    settlementId: string;
    paymentMethod: string;
    paymentMethodId: string;
    inputBalance: number = 0;
    systemBalance: number = 0;
    difference: number = 0;
}