export class ChangePasswordResource {
    userName: string;
    oldPassword: string;
    newPassword: string;
}