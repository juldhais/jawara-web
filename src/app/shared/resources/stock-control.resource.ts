export class StockControlResource {
    id: string;
    stockControlCategoryId: string;
    itemId: string;
    minimumStock: number;
    maximumStock: number;
}