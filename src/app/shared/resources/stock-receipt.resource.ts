export class StockReceiptResource {
    id: string;
    documentNumber: string;
    documentDate: Date;
    stockDeliveryId: string;
    referenceNumber: string;
    destinationLocationCode: string;
    destinationLocationId: string;
    sourceLocationCode: string;
    sourceLocationId: string;
    remarks: string;
    details: StockReceiptDetailResource[] = [];
}

export class StockReceiptDetailResource {
    id: string;
    stockReceiptId: string;
    sequence: number;
    itemCode: string;
    itemDescription: string;
    itemId: string;
    unit: string;
    ratio: number = 0;
    quantity: number = 0;
    cost: number = 0;
    subtotal: number = 0;
    remarks: string = '';
}