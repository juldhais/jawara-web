export class SupplierCategoryResource {
    id: string;
    code: string;
    description: string;
}