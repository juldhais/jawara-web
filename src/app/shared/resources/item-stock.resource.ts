export class ItemStockResource {
    itemId: string;
    locationId: string
    code: string;
    description: string;
    category: string;
    subcategory: string;
    locationCode: string;
    unit: string;
    stock: number;
    stockDetail: string;
    cost: number;
    subtotal: number;
}