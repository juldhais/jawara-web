export class ManufacturingOrderResource {
    id: string;
    documentNumber: string;
    documentDate: Date;
    locationCode: string;
    locationId: string;
    billOfMaterialCode: string;
    billOfMaterialId: string;
    parentCode: string;
    parentDescription: string;
    parentId: string;
    quantity: number = 0;
    unit: string;
    ratio: number = 0;
    cost: number = 0;
    totalCost: number = 0;
    remarks: string = '';
    dateCreated: Date;
    dateUpdated: Date;
    createdBy: string;
    updatedBy: string;
    createdById: string;
    updatedById: string;
    details: ManufacturingOrderDetailResource[] = [];
}


export class ManufacturingOrderDetailResource {
    id: string;
    manufacturingOrderId: string;
    sequence: number = 0;
    componentCode: string;
    componentName: string;
    componentId: string;
    quantity: number = 0;
    unit: string;
    ratio: number = 0;
    cost: number = 0;
    subtotalCost: number = 0;
    remarks: string = '';
}