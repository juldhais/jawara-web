export class PaymentMethodResource {
    id: string;
    code: string;
    description: string;
    sequence: number;
    purchase: boolean;
    sales: boolean;
}