export class StockControlCategoryResource {
    id: string;
    code: string;
    description: string;
}