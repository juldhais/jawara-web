export class PurchasePaymentResource {
    id: string;
    documentNumber: string;
    documentDate: Date;
    referenceNumber: string;
    direct: boolean;
    locationCode: string;
    locationId: string;
    supplierCode: string;
    supplierDescription: string;
    supplierId: string;
    totalPayment: number = 0;
    totalReturn: number = 0;
    amountPaid: number = 0;
    paymentMethod: string;
    paymentMethodId: string;
    remarks: string;
    details: PurchasePaymentDetailResource[] = [];
    returns: PurchasePaymentReturnResource[] = [];
}

export class PurchasePaymentDetailResource {
    id: string;
    purchasePaymentId: string;
    documentNumber: string;
    documentDate: Date;
    purchaseReceiptId: string;
    balance: number = 0;
    payment: number = 0;
    remaining: number = 0;
    remarks: string;
}

export class PurchasePaymentReturnResource {
    id: string;
    check: boolean;
    documentNumber: string;
    documentDate: Date;
    referenceNumber: string;
    location: string;
    totalAfterTax: number;
}