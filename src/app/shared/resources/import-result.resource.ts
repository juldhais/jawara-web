export class ImportResultResource {
    isError: boolean = false;
    errorMessages: string[] = [];
}