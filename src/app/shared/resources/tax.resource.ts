export class TaxResource {
    id: string;
    code: string;
    description: string;
    type: string;
    rate: number;
    amount: number;
    remarks: number;
}