export class UserResource {
    id: string;
    userName: string;
    password: string;
    description: string;
    roleCode: string;
    roleId: string;

    defaultLocationCode: string;
    defaultLocationId: string;
}