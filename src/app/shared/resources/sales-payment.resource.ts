export class SalesPaymentResource {
    id: string;
    documentNumber: string;
    documentDate: Date;
    referenceNumber: string;
    direct: boolean;
    locationCode: string;
    locationId: string;
    customerCode: string;
    customerDescription: string;
    customerId: string;
    totalPayment: number = 0;
    totalReturn: number = 0;
    amountPaid: number = 0;
    paymentMethod: string;
    paymentMethodId: string;
    remarks: string;
    details: SalesPaymentDetailResource[] = [];
    returns: SalesPaymentReturnResource[] = [];
}

export class SalesPaymentDetailResource {
    id: string;
    salesPaymentId: string;
    documentNumber: string;
    documentDate: Date;
    salesDeliveryId: string;
    balance: number = 0;
    payment: number = 0;
    remaining: number = 0;
    remarks: string;
}

export class SalesPaymentReturnResource {
    id: string;
    check: boolean;
    documentNumber: string;
    documentDate: Date;
    referenceNumber: string;
    location: string;
    totalAfterTax: number;
}