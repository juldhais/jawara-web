export class ItemCategoryResource {
    id: string;
    code: string;
    description: string;
}