export class PagingResult<T>
{
    data: T[];
    page: number;
    size: number;
    totalPage: number;
    totalData: number;
}