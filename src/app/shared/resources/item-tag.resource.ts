export class ItemTagResource {
    id: string;
    code: string;
    description: string;
}