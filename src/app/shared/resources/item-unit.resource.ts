export class ItemUnitResource {
    id: string;
    itemId: string;
    unit: string;
    ratio: number;
}