export class PrintResource {
    type: string;
    module: string;
    data: any;
}