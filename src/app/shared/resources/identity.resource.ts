import { UserResource } from './user.resource';
import { RoleResource } from './role.resource';

export class IdentityResource {
    user: UserResource;
    role: RoleResource;
    token: string;
}