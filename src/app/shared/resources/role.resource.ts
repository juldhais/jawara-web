export class RoleResource {
    id: string;
    code: string;
    description: string;
    isAdministrator: boolean;
    privileges: string[] = [];
}