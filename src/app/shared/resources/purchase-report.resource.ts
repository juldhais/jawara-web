export class PurchaseReportResource {
    startDate: Date;
    endDate: Date;
    location: string;
    supplierCategory: string;
    supplier: string;
    user: string;
    status: string;
    totalBeforeTax: number;
    totalTax: number;
    totalAfterTax: number;
    totalPayment: number;
    balance: number;
    data: PurchaseReportData[] = [];
}

export class PurchaseReportData {
    id: string;
    documentNumber: string;
    documentDate: Date;
    referenceNumber: string;
    status: string;
    purchaseOrderDocumentNumber: string;
    purchaseOrderId: string;
    location: string;
    locationId: string;
    supplier: string;
    supplierId: string;
    totalBeforeTax: number = 0;
    totalTax: number = 0;
    totalAfterTax: number = 0;
    totalPayment: number = 0;
    balance: number = 0;
    remarks: string;
}

export class PurchaseReportParam {
    startDate: Date;
    endDate: Date;
    locationId: string;
    supplierCategoryId: string;
    supplierId: string;
    userId: string;
    status: string = '';
}