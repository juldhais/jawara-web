export class StockDeliveryResource {
    id: string;
    documentNumber: string;
    documentDate: Date;
    stockOrderId: string;
    referenceNumber: string;
    status: string;
    destinationLocationCode: string;
    destinationLocationId: string;
    sourceLocationCode: string;
    sourceLocationId: string;
    remarks: string;
    details: StockDeliveryDetailResource[] = [];
}

export class StockDeliveryDetailResource {
    id: string;
    stockDeliveryId: string;
    sequence: number;
    itemCode: string;
    itemDescription: string;
    itemId: string;
    unit: string;
    ratio: number = 0;
    quantity: number = 0;
    cost: number = 0;
    subtotal: number = 0;
    remarks: string = '';
}