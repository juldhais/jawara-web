export class UnitResource {
    id: string;
    code: string;
    description: string;
    sequence: number;
}