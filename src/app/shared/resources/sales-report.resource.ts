export class SalesReportResource {
    startDate: Date;
    endDate: Date;
    location: string;
    customerCategory: string;
    customer: string;
    user: string;
    status: string;
    totalBeforeTax: number;
    totalTax: number;
    totalAfterTax: number;
    totalPayment: number;
    balance: number;
    data: SalesReportData[] = [];
}

export class SalesReportData {
    id: string;
    documentNumber: string;
    documentDate: Date;
    referenceNumber: string;
    status: string;
    salesOrderDocumentNumber: string;
    salesOrderId: string;
    location: string;
    locationId: string;
    customer: string;
    customerId: string;
    totalBeforeTax: number = 0;
    totalTax: number = 0;
    totalAfterTax: number = 0;
    totalPayment: number = 0;
    balance: number = 0;
    remarks: string;
}

export class SalesReportParam {
    startDate: Date;
    endDate: Date;
    locationId: string;
    customerCategoryId: string;
    customerId: string;
    userId: string;
    status: string = '';
}