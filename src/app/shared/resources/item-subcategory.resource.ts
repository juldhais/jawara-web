export class ItemSubcategoryResource {
    id: string;
    code: string;
    description: string;
}