export class BillOfMaterialResource {
    id: string;
    code: string;
    description: string;
    parentCode: string;
    parentDescription: string;
    parentId: string;
    parentQuantity: number;
    parentUnit: string;
    parentRatio: number;
    details: BillOfMaterialDetailResource[] = [];
}

export class BillOfMaterialDetailResource {
    id: string;
    billOfMaterialId: string;
    sequence: number;
    componentCode: string;
    componentDescription: string;
    componentId: string;
    componentQuantity: number;
    componentUnit: string;
    componentRatio: number;
}