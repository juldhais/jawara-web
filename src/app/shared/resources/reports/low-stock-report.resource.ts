export class LowStockReportResource {
    page: number;
    size: number;
    totalPage: number;
    totalData: number;
    data: LowStockReportData[] = [];
}

export class LowStockReportData {
    itemId: string;
    code: string;
    name: string;
    category: string;
    subcategory: string;
    tag: string;
    locationCode: string;
    locationName: string;
    supplier: string;
    unit: string;
    stock: number;
    stockDetail: string;
    cost: number;
    purchasePrice: number;
    minimumStock: number;
    maximumStock: number;
    reorderQuantity: number;
}

export class LowStockReportParam {
    locationId: string;
    itemCategoryId: string;
    itemSubcategoryId: string;
    itemTag: string;
    supplierId: string;
    itemId: string;
    page: number;
    size: number;
}