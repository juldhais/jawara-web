export class StockMovementReportResource {
    startDate: Date;
    endDate: Date;
    location: string;
    item: string;
    data: StockMovementReportData[];
}

export class StockMovementReportData {
    sequence: number;
    date: Date;
    transaction: string;
    documentNumber: string;
    remarks: string;
    location: string;
    in: number;
    out: number;
    balance: number;
}

export class StockMovementReportParam {
    startDate: Date;
    endDate: Date;
    locationId: string;
    itemId: string;
}