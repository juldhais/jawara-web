export class StockDeliveryReportResource {
    startDate: Date;
    endDate: Date;
    destination: string;
    source: string;
    user: string;
    itemCategory: string;
    itemSubcategory: string;
    itemTag: string;
    item: string;
    data: StockDeliveryReportData[] = [];
}

export class StockDeliveryReportData {
    documentNumber: string;
    documentDate: Date;
    destination: string;
    source: string;
    itemCode: string;
    itemName: string;
    unit: string;
    ratio: number;
    quantity: number;
}

export class StockDeliveryReportParam {
    startDate: Date;
    endDate: Date;
    destinationId: string;
    sourceId: string;
    userId: string;
    itemCategoryId: string;
    itemSubcategoryId: string;
    itemTag: string;
    itemId: string;
}