export class PurchaseDetailReportResource {
    startDate: Date;
    endDate: Date;
    location: string;
    supplierCategory: string;
    supplier: string;
    user: string;
    status: string;
    itemCategory: string;
    itemSubcategory: string;
    itemTag: string;
    item: string;
    totalBeforeTax: number;
    totalTax: number;
    totalAfterTax: number;
    data: PurchaseDetailReportData[] = [];
}

export class PurchaseDetailReportData {
    id: string;
    documentNumber: string;
    documentDate: Date;
    referenceNumber: string;
    location: string;
    supplier: string;
    sequence: number;
    itemCode: string;
    itemName: string;
    itemId: string;
    unit: string;
    ratio: number = 0;
    quantity: number = 0;
    price: number = 0;
    discountPercent1: number = 0;
    discountPercent2: number = 0;
    discountAmount: number = 0;
    taxPercent: number = 0;
    taxAmount: number = 0;
    subtotalBeforeTax: number = 0;
    subtotalTax: number = 0;
    subtotalAfterTax: number = 0;
    remarks: string;
}

export class PurchaseDetailReportParam {
    startDate: Date;
    endDate: Date;
    locationId: string;
    supplierCategoryId: string;
    supplierId: string;
    userId: string;
    status: string = '';
    itemCategoryId: string;
    itemSubcategoryId: string;
    itemTag: string;
    itemId: string;
}