export class PurchasePaymentReportResource {
    startDate: Date;
    endDate: Date;
    location: string;
    supplierCategory: string;
    supplier: string;
    paymentMethod: string;
    user: string;
    totalPayment: number;
    data: PurchasePaymentReportData[] = [];
}

export class PurchasePaymentReportData {
    id: string;
    documentNumber: string;
    documentDate: Date;
    location: string;
    supplier: string;
    purchaseReceiptDocumentNumber: string;
    paymentMethod: string;
    payment: number;
    remarks: string;
}

export class PurchasePaymentReportParam {
    startDate: Date;
    endDate: Date;
    locationId: string;
    supplierCategoryId: string;
    supplierId: string;
    paymentMethodId: string;
    userId: string;
}