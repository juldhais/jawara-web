export class StockReportResource {
    page: number;
    size: number;
    totalPage: number;
    totalData: number;
    data: StockReportData[] = [];
}

export class StockReportData {
    itemId: string;
    code: string;
    name: string;
    category: string;
    subcategory: string;
    tag: string;
    locationCode: string;
    locationName: string;
    unit: string;
    stock: number;
    stockDetail: string;
    cost: number;
    subtotal: number;
}

export class StockReportParam {
    locationId: string;
    itemCategoryId: string;
    itemSubcategoryId: string;
    itemTag: string;
    supplierId: string;
    itemId: string;
    sort: string;
    page: number;
    size: number;
}