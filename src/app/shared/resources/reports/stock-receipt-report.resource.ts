export class StockReceiptReportResource {
    startDate: Date;
    endDate: Date;
    destination: string;
    source: string;
    user: string;
    itemCategory: string;
    itemSubcategory: string;
    itemTag: string;
    item: string;
    data: StockReceiptReportData[] = [];
}

export class StockReceiptReportData {
    documentNumber: string;
    documentDate: Date;
    destination: string;
    source: string;
    itemCode: string;
    itemName: string;
    unit: string;
    ratio: number;
    quantity: number;
}

export class StockReceiptReportParam {
    startDate: Date;
    endDate: Date;
    destinationId: string;
    sourceId: string;
    userId: string;
    itemCategoryId: string;
    itemSubcategoryId: string;
    itemTag: string;
    itemId: string;
}