export class SalesDetailReportResource {
    startDate: Date;
    endDate: Date;
    location: string;
    customerCategory: string;
    customer: string;
    user: string;
    status: string;
    itemCategory: string;
    itemSubcategory: string;
    itemTag: string;
    item: string;
    totalBeforeTax: number;
    totalTax: number;
    totalAfterTax: number;
    data: SalesDetailReportData[] = [];
}

export class SalesDetailReportData {
    id: string;
    documentNumber: string;
    documentDate: Date;
    referenceNumber: string;
    location: string;
    customer: string;
    sequence: number;
    itemCode: string;
    itemName: string;
    itemId: string;
    unit: string;
    ratio: number = 0;
    quantity: number = 0;
    price: number = 0;
    discountPercent1: number = 0;
    discountPercent2: number = 0;
    discountAmount: number = 0;
    taxPercent: number = 0;
    taxAmount: number = 0;
    subtotalBeforeTax: number = 0;
    subtotalTax: number = 0;
    subtotalAfterTax: number = 0;
    cost: number = 0;
    subtotalCost: number = 0;
    remarks: string;
}

export class SalesDetailReportParam {
    startDate: Date;
    endDate: Date;
    locationId: string;
    customerCategoryId: string;
    customerId: string;
    userId: string;
    status: string = '';
    itemCategoryId: string;
    itemSubcategoryId: string;
    itemTag: string;
    itemId: string;
}