export class SalesPaymentReportResource {
    startDate: Date;
    endDate: Date;
    location: string;
    customerCategory: string;
    customer: string;
    paymentMethod: string;
    user: string;
    totalPayment: number;
    data: SalesPaymentReportData[] = [];
}

export class SalesPaymentReportData {
    id: string;
    documentNumber: string;
    documentDate: Date;
    location: string;
    customer: string;
    salesDeliveryDocumentNumber: string;
    paymentMethod: string;
    payment: number;
    remarks: string;
}

export class SalesPaymentReportParam {
    startDate: Date;
    endDate: Date;
    locationId: string;
    customerCategoryId: string;
    customerId: string;
    paymentMethodId: string;
    userId: string;
}