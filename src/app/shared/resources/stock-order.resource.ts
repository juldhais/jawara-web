export class StockOrderResource {
    id: string;
    documentNumber: string;
    documentDate: Date;
    referenceNumber: string;
    status: string;
    destinationLocationCode: string;
    destinationLocationId: string;
    sourceLocationCode: string;
    sourceLocationId: string;
    remarks: string;
    details: StockOrderDetailResource[] = [];
}

export class StockOrderDetailResource {
    id: string;
    stockOrderId: string;
    sequence: number;
    itemCode: string;
    itemDescription: string;
    itemId: string;
    unit: string;
    ratio: number = 0;
    quantity: number = 0;
    cost: number = 0;
    subtotal: number = 0;
    remarks: string = '';
}