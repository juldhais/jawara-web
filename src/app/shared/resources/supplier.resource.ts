export class SupplierResource {
    id: string;
    code: string;
    description: string;
    supplierCategoryCode: string;
    supplierCategoryId: string;

    address1: string;
    phone1: string;
    email1: string;
    website1: string;
    contact1: string;

    address2: string;
    phone2: string;
    email2: string;
    website2: string;
    contact2: string;

    image: string;
    remarks: string;
}