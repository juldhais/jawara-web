export class CustomerCategoryResource {
    id: string;
    code: string;
    description: string;
}