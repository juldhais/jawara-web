import { Component } from '@angular/core';

@Component({
  templateUrl: 'home.component.html',
  styleUrls: [ './home.component.scss' ]
})

export class HomeComponent {

    datasource = [
        {
            id: 1,
            code: '001',
            description: 'Product 001'
        },
        {
            id: 2,
            code: '002',
            description: 'Product 002'
        }
    ];

  constructor() {}
}
