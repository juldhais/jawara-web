import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { SupplierService } from 'src/app/shared/services/supplier.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { SupplierCategoryResource } from 'src/app/shared/resources/supplier-category.resource';
import { SupplierCategoryService } from 'src/app/shared/services/supplier-category.service';
import { SupplierResource } from 'src/app/shared/resources/supplier.resource';
import { Router } from '@angular/router';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';

@Component({
    selector: 'app-supplier',
    templateUrl: './supplier.component.html',
    styleUrls: ['./supplier.component.scss']
})
export class SupplierComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: CustomStore;
    popupTitle: string;
    popupVisible: boolean;
    loadingVisible: boolean;
    editMode: string;
    editResource = new SupplierResource();

    listCategory: SupplierCategoryResource[];

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode:"contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.save()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type:"normal",
                stylingMode:"contained"
            },
            toolbar: 'bottom',
            onClick: () => this.popupVisible = false
        }
    ];

    constructor(
        private supplierService: SupplierService,
        private supplierCategoryService: SupplierCategoryService,
        private router: Router) { }

    async ngOnInit() {
        DomHelper.setPageTitle('SUPPLIER');
        this.dataSource = this.supplierService.getCustomStore();
        this.listCategory = (await this.supplierCategoryService.getList()).data;
    }

    create() {
        this.editMode = 'create';
        this.editResource = new SupplierResource();
        this.popupTitle = "Create Supplier";
        this.popupVisible = true;
    }

    async update(id) {
        this.editMode = 'update';
        this.editResource = await this.supplierService.get(id);

        this.popupTitle = "Update Supplier";
        this.popupVisible = true;
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    async save() {
        try {
            this.loadingVisible = true;
    
            if (this.editMode == 'create')
                await this.supplierService.create(this.editResource);
            else if (this.editMode == 'update')
                await this.supplierService.update(this.editResource);
            
            this.dataGrid.instance.refresh();
            this.popupVisible = false;
            notify('Data saved successfully.', 'success', 2000);
        } catch (error) {
            if ('error' in error) notify(error.error.message, 'error', 3000);
            else notify(error.message, 'error', 3000);
        }

        this.loadingVisible = false;
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "Supplier";
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }

    navigateTo(url: string) {
        this.router.navigateByUrl(url);
    }
}
