import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { UserService } from 'src/app/shared/services/user.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { RoleResource } from 'src/app/shared/resources/role.resource';
import { RoleService } from 'src/app/shared/services/role.service';
import { UserResource } from 'src/app/shared/resources/user.resource';
import { Router } from '@angular/router';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';
import { LocationResource } from 'src/app/shared/resources/location.resource';
import { LocationService } from 'src/app/shared/services/location.service';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: CustomStore;
    popupTitle: string;
    popupVisible: boolean;
    loadingVisible: boolean;
    editMode: string;
    editResource = new UserResource();

    listRole: RoleResource[];
    listLocation: LocationResource[]; 

    passwordMode = 'password';
    passwordButton = {
        icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAB7klEQVRYw+2YP0tcQRTFz65xFVJZpBBS2O2qVSrRUkwqYfUDpBbWQu3ELt/HLRQ/Q8RCGxVJrRDEwj9sTATxZ/Hugo4zL/NmV1xhD9xi59177pl9986fVwLUSyi/tYC+oL6gbuNDYtyUpLqkaUmfJY3a+G9JZ5J2JW1J2ivMDBSxeWCfeBxYTHSOWMcRYLOAEBebxtEVQWPASQdi2jgxro4E1YDTQIJjYM18hszGbew4EHNq/kmCvgDnHtI7YBko58SWgSXg1hN/btyFBM0AlwExczG1YDZrMS4uLUeUoDmgFfjLGwXEtG05wNXyTc4NXgzMCOAIGHD8q0ATuDZrempkwGJ9+AfUQ4K+A/eEseqZ/UbgdUw4fqs5vPeW+5mgBvBAPkLd8cPju+341P7D/WAaJGCdOFQI14kr6o/zvBKZYz11L5Okv5KGA89Kzu9K0b0s5ZXt5PjuOL6TRV5ZalFP4F+rrnhZ1Cs5vN6ijmn7Q162/ThZq9+YNW3MbfvDAOed5cxdGL+RFaUPKQtjI8DVAr66/u9i6+jJzTXm+HFEVqxVYBD4SNZNKzk109HxoycPaG0bIeugVDTp4hH2qdXJDu6xOAAWiuQoQdLHhvY1aEZSVdInG7+Q9EvSz9RrUKqgV0PP3Vz7gvqCOsUj+CxC9LB1Dc8AAAASdEVYdEVYSUY6T3JpZW50YXRpb24AMYRY7O8AAAAASUVORK5CYII=",
        onClick: () => {
            this.passwordMode = this.passwordMode === "text" ? "password" : "text";
        }
    };

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode:"contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.save()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type:"normal",
                stylingMode:"contained"
            },
            toolbar: 'bottom',
            onClick: () => this.popupVisible = false
        }
    ];

    constructor(
        private userService: UserService,
        private roleService: RoleService,
        private locationService: LocationService,
        private router: Router) { }

    async ngOnInit() {
        DomHelper.setPageTitle('USER');
        this.dataSource = this.userService.getCustomStore();

        this.listRole = (await this.roleService.getList()).data;
        this.listLocation = (await this.locationService.getList()).data;
    }

    create() {
        this.editMode = 'create';
        this.editResource = new UserResource();
        this.popupTitle = "Create User";
        this.popupVisible = true;
    }

    async update(id) {
        this.editMode = 'update';
        this.editResource = await this.userService.get(id);

        this.popupTitle = "Update User";
        this.popupVisible = true;
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    async save() {
        try {
            this.loadingVisible = true;
    
            if (this.editMode == 'create')
                await this.userService.create(this.editResource);
            else if (this.editMode == 'update')
                await this.userService.update(this.editResource);
            
            this.dataGrid.instance.refresh();
            this.popupVisible = false;
            notify('Data saved successfully.', 'success', 2000);
        } catch (error) {
            if ('error' in error) notify(error.error.message, 'error', 3000);
            else notify(error.message, 'error', 3000);
        }

        this.loadingVisible = false;
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "User";
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }

    navigateTo(url: string) {
        this.router.navigateByUrl(url);
    }
}
