import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { CustomerService } from 'src/app/shared/services/customer.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { CustomerCategoryResource } from 'src/app/shared/resources/customer-category.resource';
import { CustomerCategoryService } from 'src/app/shared/services/customer-category.service';
import { CustomerResource } from 'src/app/shared/resources/customer.resource';
import { Router } from '@angular/router';
import { PriceControlCategoryResource } from 'src/app/shared/resources/price-control-category.resource';
import { LocationResource } from 'src/app/shared/resources/location.resource';
import { PriceControlCategoryService } from 'src/app/shared/services/price-control-category.service';
import { LocationService } from 'src/app/shared/services/location.service';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';
import { Helper } from 'src/app/shared/helpers/helper';

@Component({
    selector: 'app-customer',
    templateUrl: './customer.component.html',
    styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: CustomStore;
    popupTitle: string;
    popupVisible: boolean;
    loadingVisible: boolean;
    editMode: string;
    editResource = new CustomerResource();

    locationDataSource: CustomStore;
    listCategory: CustomerCategoryResource[];
    listPriceControlCategory: PriceControlCategoryResource[];

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode:"contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.save()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type:"normal",
                stylingMode:"contained"
            },
            toolbar: 'bottom',
            onClick: () => this.popupVisible = false
        }
    ];

    constructor(
        private customerService: CustomerService,
        private customerCategoryService: CustomerCategoryService,
        private priceControlCategoryService: PriceControlCategoryService,
        private locationService: LocationService,
        private router: Router) { }

    async ngOnInit() {
        DomHelper.setPageTitle('CUSTOMER');
        this.dataSource = this.customerService.getCustomStore();

        this.locationDataSource = this.locationService.getCustomStore();
        this.listCategory = (await this.customerCategoryService.getList()).data;
        this.listPriceControlCategory = (await this.priceControlCategoryService.getList()).data;
    }

    create() {
        this.editMode = 'create';
        this.editResource = new CustomerResource();
        this.editResource.locationId = Helper.getIdentity().user.defaultLocationId;
        this.popupTitle = "Create Customer";
        this.popupVisible = true;
    }

    async update(id) {
        this.editMode = 'update';
        this.editResource = await this.customerService.get(id);

        this.popupTitle = "Update Customer";
        this.popupVisible = true;
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    async save() {
        try {
            this.loadingVisible = true;
    
            if (this.editMode == 'create')
                await this.customerService.create(this.editResource);
            else if (this.editMode == 'update')
                await this.customerService.update(this.editResource);
            
            this.dataGrid.instance.refresh();
            this.popupVisible = false;
            notify('Data saved successfully.', 'success', 2000);
        } catch (error) {
            if ('error' in error) notify(error.error.message, 'error', 3000);
            else notify(error.message, 'error', 3000);
        }

        this.loadingVisible = false;
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "Customer";
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }

    navigateTo(url: string) {
        this.router.navigateByUrl(url);
    }
}
