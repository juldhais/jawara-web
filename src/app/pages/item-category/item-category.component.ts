import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { ItemCategoryService } from 'src/app/shared/services/item-category.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';

@Component({
    selector: 'app-item-category',
    templateUrl: './item-category.component.html',
    styleUrls: ['./item-category.component.scss']
})
export class ItemCategoryComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: any;
    popupTitle: string;

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode: "contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.dataGrid.instance.saveEditData()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type: "normal",
                stylingMode: "contained"
            },
            toolbar: 'bottom',
            onClick: () => this.dataGrid.instance.cancelEditData()
        }
    ];

    constructor(private itemCategoryService: ItemCategoryService) { }

    async ngOnInit() {
        DomHelper.setPageTitle('ITEM CATEGORY');
        this.dataSource = this.itemCategoryService.getCustomStore();
    }

    create() {
        this.popupTitle = 'Create Item Category';
        this.dataGrid.instance.addRow();
    }

    update(id) {
        this.popupTitle = 'Update Item Category';
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.editRow(index);
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "ItemCategory";
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }
}
