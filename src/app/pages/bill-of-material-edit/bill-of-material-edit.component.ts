import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { BillOfMaterialResource, BillOfMaterialDetailResource } from 'src/app/shared/resources/bill-of-material.resource';
import { BillOfMaterialService } from 'src/app/shared/services/bill-of-material.service';
import { ItemService } from 'src/app/shared/services/item.service';
import { confirm } from 'devextreme/ui/dialog';
import notify from 'devextreme/ui/notify';
import { ItemResource } from 'src/app/shared/resources/item.resource';
import { DxDataGridComponent } from 'devextreme-angular';
import { ItemUnitResource } from 'src/app/shared/resources/item-unit.resource';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';
import { Helper } from 'src/app/shared/helpers/helper';

@Component({
    selector: 'app-bill-of-material-edit',
    templateUrl: './bill-of-material-edit.component.html',
    styleUrls: ['./bill-of-material-edit.component.scss']
})
export class BillOfMaterialEditComponent implements OnInit {
    @ViewChild('dataGridDetail', { static: false })
    dataGridDetail: DxDataGridComponent;

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode:"contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.saveDetail()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type:"normal",
                stylingMode:"contained"
            },
            toolbar: 'bottom',
            onClick: () => this.popupVisible = false
        }
    ];

    id: string;
    mode: string;
    popupTitle: string;
    popupVisible: boolean;
    loadingVisible: boolean;
    detailEditMode: string;

    editResource = new BillOfMaterialResource();
    editDetail = new BillOfMaterialDetailResource();

    parentItemDataSource: CustomStore;
    componentItemDataSource: CustomStore;

    parentItem: ItemResource;
    parentUnits: ItemUnitResource[] = [];
    componentItem: ItemResource;
    componentUnits: ItemUnitResource[] = [];

    constructor(
        private bomService: BillOfMaterialService,
        private itemService: ItemService,
        private router: Router,
        private route: ActivatedRoute,
        ) { }

    ngOnInit() {
        this.parentItemDataSource = this.itemService.getLookupCustomStore('manufacturable');
        this.componentItemDataSource = this.itemService.getLookupCustomStore();
        
        this.route.params.subscribe(async params => {
            this.mode = params['mode'];

            if (this.mode == 'create') {
                this.editResource = new BillOfMaterialResource();
                this.editResource.parentQuantity = 1;
                this.editResource.parentRatio = 1;
                DomHelper.setPageTitle('CREATE BILL OF MATERIAL');
            }
            else if (this.mode == 'update') {
                this.id = params['id'];
                this.editResource = await this.bomService.get(this.id);
                DomHelper.setPageTitle('UPDATE BILL OF MATERIAL');
            }
        });
    }

    async parentItemChange() {
        if (!this.editResource.parentId) return;
        this.parentItem = await this.itemService.get(this.editResource.parentId);
        this.parentUnits = this.itemService.getUnits(this.parentItem);
        const parentUnit = this.parentUnits[0];
        if (parentUnit) {
            this.editResource.parentUnit = parentUnit.unit;
            this.editResource.parentRatio = parentUnit.ratio;
        }
    }

    parentUnitChange() {
        const parentUnit = this.parentUnits.filter(x => x.unit == this.editResource.parentUnit)[0];
        if (parentUnit) this.editResource.parentRatio = parentUnit.ratio;
    }

    async componentItemChange() {
        if (this.editDetail.componentId) {
            this.componentItem = await this.itemService.get(this.editDetail.componentId);
            this.componentUnits = this.itemService.getUnits(this.componentItem);
            
            this.editDetail.componentRatio = 1;

            const componentUnit = this.componentUnits[0];
            if (componentUnit) {
                this.editDetail.componentUnit = componentUnit.unit;
                this.editDetail.componentRatio = componentUnit.ratio;
            }
        }
    }

    componentUnitChange() {
        const componentUnit = this.componentUnits.filter(x => x.unit == this.editDetail.componentUnit)[0];
        if (componentUnit)
            this.editDetail.componentRatio = componentUnit.ratio;
        else this.editDetail.componentRatio = 1;
    }

    actionClick(e, data) {
        if (e.itemData == 'Update') this.updateDetail(data);
        else if (e.itemData == 'Delete') this.deleteDetail(data);
    }

    createDetail() {
        this.detailEditMode = 'create';
        this.popupTitle = 'Add Component';
        
        this.editDetail = new BillOfMaterialDetailResource();

        let sequence = 0;
        if (this.editResource.details.length > 0)
            sequence = Math.max(...this.editResource.details.map(x => x.sequence));
        this.editDetail.sequence = sequence + 1;

        this.editDetail.componentQuantity = 1;
        this.editDetail.componentRatio = 1;

        this.popupVisible = true;
    }

    async updateDetail(data: BillOfMaterialDetailResource) {
        this.detailEditMode = 'update';
        this.popupTitle = 'Update Component';

        this.editDetail = JSON.parse(JSON.stringify(data));

        this.componentItem = await this.itemService.get(this.editDetail.componentId);
        this.componentUnits = this.itemService.getUnits(this.componentItem);

        this.popupVisible = true;
    }

    async deleteDetail(data: BillOfMaterialDetailResource) {
        const result = await confirm('Are you sure?', 'Delete Component');
        if (result) {
            const index = this.editResource.details.findIndex(x => x.sequence == data.sequence);
            this.editResource.details.splice(index, 1);
        }
    }

    saveDetail() {
        try {
            if (this.editDetail.componentQuantity == 0)
                throw new Error('Component Quantity cannot be empty.');

            if (this.editDetail.componentRatio == 0)
                throw new Error('Component Ratio cannot be empty.');

            this.editDetail.componentCode = this.componentItem.code;
            this.editDetail.componentDescription = this.componentItem.description;

            if (this.detailEditMode == 'create') {
                let detail = new BillOfMaterialDetailResource();
                Helper.copyObject(detail, this.editDetail);
                this.editResource.details.push(detail);
            }
            else if (this.detailEditMode == 'update') {
                let detail = this.editResource.details.filter(x => x.sequence == this.editDetail.sequence)[0];
                Helper.copyObject(detail, this.editDetail);
            }

            this.popupVisible = false;
        } catch (error) {
            notify(error.message, 'error', 3000);
        }
    }

    async save() {
        try {
            this.loadingVisible = true;

            if (this.mode == 'create')
                await this.bomService.create(this.editResource);
            else if (this.mode == 'update')
                await this.bomService.update(this.editResource);
            
            notify('Data saved successfully.', 'success', 2000);

            this.router.navigateByUrl('bill-of-material');
        } catch (error) {
            if ('error' in error) notify(error.error.message, 'error', 3000);
            else notify(error.message, 'error', 3000);
        }

        this.loadingVisible = false;
    }

    async cancel() {
        const result = await confirm('Are you sure want to cancel?', 'Bill of Material');
        if (result) this.router.navigateByUrl('bill-of-material');
    }

}
