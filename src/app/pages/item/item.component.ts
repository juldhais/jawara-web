import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { ItemService } from 'src/app/shared/services/item.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { ItemCategoryResource } from 'src/app/shared/resources/item-category.resource';
import { ItemSubcategoryResource } from 'src/app/shared/resources/item-subcategory.resource';
import { ItemTagResource } from 'src/app/shared/resources/item-tag.resource';
import { ItemCategoryService } from 'src/app/shared/services/item-category.service';
import { ItemSubcategoryService } from 'src/app/shared/services/item-subcategory.service';
import { ItemTagService } from 'src/app/shared/services/item-tag.service';
import { ItemResource } from 'src/app/shared/resources/item.resource';
import { UnitResource } from 'src/app/shared/resources/unit.resource';
import { UnitService } from 'src/app/shared/services/unit.service';
import { SupplierService } from 'src/app/shared/services/supplier.service';
import { Router } from '@angular/router';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: CustomStore;
    popupTitle: string;
    popupVisible: boolean;
    loadingVisible: boolean;
    editMode: string;
    editResource = new ItemResource();
    editTag: string[] = [];

    listCategory: ItemCategoryResource[];
    listSubcategory: ItemSubcategoryResource[];
    listTag: ItemTagResource[] = [];
    listUnit: UnitResource[] = [];

    supplierDataSource: CustomStore;

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode:"contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.save()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type:"normal",
                stylingMode:"contained"
            },
            toolbar: 'bottom',
            onClick: () => this.popupVisible = false
        }
    ];

    constructor(
        private itemService: ItemService,
        private itemCategoryService: ItemCategoryService,
        private itemSubcategoryService: ItemSubcategoryService,
        private itemTagService: ItemTagService,
        private unitService: UnitService,
        private supplierService: SupplierService,
        private router: Router) { }

    async ngOnInit() {
        DomHelper.setPageTitle('ITEM');

        this.dataSource = this.itemService.getCustomStore();
        this.supplierDataSource = this.supplierService.getCustomStore();

        this.listCategory = (await this.itemCategoryService.getList()).data;
        this.listSubcategory = (await this.itemSubcategoryService.getList()).data;
        this.listTag = (await this.itemTagService.getList()).data;
        this.listUnit = (await this.unitService.getList()).data;
    }

    create() {
        this.editMode = 'create';
        this.editResource = new ItemResource();
        this.editTag = [];
        this.popupTitle = "Create Item";
        this.popupVisible = true;
    }

    async update(id) {
        this.editMode = 'update';
        this.editResource = await this.itemService.get(id);
        this.editTag = [];

        if (this.editResource.tag)
            this.editTag = this.editResource.tag.split(', ');

        this.popupTitle = "Update Item";
        this.popupVisible = true;
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    async save() {
        try {
            this.loadingVisible = true;
            this.editResource.tag = this.editTag.join(', ');
    
            if (this.editMode == 'create')
                await this.itemService.create(this.editResource);
            else if (this.editMode == 'update')
                await this.itemService.update(this.editResource);
            
            this.dataGrid.instance.refresh();
            this.popupVisible = false;
            notify('Data saved successfully.', 'success', 2000);
        } catch (error) {
            if ('error' in error) notify(error.error.message, 'error', 3000);
            else notify(error.message, 'error', 3000);
        }

        this.loadingVisible = false;
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "Item";
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }

    navigateTo(url: string) {
        this.router.navigateByUrl(url);
    }
}
