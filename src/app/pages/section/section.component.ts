import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { SectionService } from 'src/app/shared/services/section.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';

@Component({
    selector: 'app-section',
    templateUrl: './section.component.html',
    styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: any;
    popupTitle: string;

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode: "contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.dataGrid.instance.saveEditData()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type: "normal",
                stylingMode: "contained"
            },
            toolbar: 'bottom',
            onClick: () => this.dataGrid.instance.cancelEditData()
        }
    ];

    constructor(private sectionService: SectionService) { }

    async ngOnInit() {
        DomHelper.setPageTitle('SECTION');
        this.dataSource = this.sectionService.getCustomStore();
    }

    create() {
        this.popupTitle = 'Create Section';
        this.dataGrid.instance.addRow();
    }

    update(id) {
        this.popupTitle = 'Update Section';
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.editRow(index);
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "Section";
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }
}
