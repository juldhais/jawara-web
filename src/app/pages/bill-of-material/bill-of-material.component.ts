import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { BillOfMaterialService } from 'src/app/shared/services/bill-of-material.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { Router } from '@angular/router';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';

@Component({
    selector: 'app-bill-of-material',
    templateUrl: './bill-of-material.component.html',
    styleUrls: ['./bill-of-material.component.scss']
})
export class BillOfMaterialComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: any;

    constructor(
        private bomService: BillOfMaterialService,
        private router: Router) { }

    async ngOnInit() {
        DomHelper.setPageTitle('BILL OF MATERIAL');
        this.dataSource = this.bomService.getCustomStore();
    }

    create() {
        this.router.navigateByUrl(`bill-of-material/create`);
    }

    update(id) {
        this.router.navigateByUrl(`bill-of-material/update/${id}`);
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "BillOfMaterial";
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }
}
