import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { Router } from '@angular/router';
import { PriceControlCategoryResource } from 'src/app/shared/resources/price-control-category.resource';
import { TaxResource } from 'src/app/shared/resources/tax.resource';
import { PriceControlCategoryService } from 'src/app/shared/services/price-control-category.service';
import { TaxService } from 'src/app/shared/services/tax.service';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';
import { StockControlCategoryResource } from 'src/app/shared/resources/stock-control-category.resource';
import { StockControlCategoryService } from 'src/app/shared/services/stock-control-category.service';

@Component({
    selector: 'app-item',
    templateUrl: './tax.component.html',
    styleUrls: ['./tax.component.scss']
})
export class TaxComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: CustomStore;
    popupTitle: string;
    popupVisible: boolean;
    loadingVisible: boolean;
    editMode: string;
    editResource = new TaxResource();

    listPriceControlCategory: PriceControlCategoryResource[];
    listStockControlCategory: StockControlCategoryResource[];

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode:"contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.save()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type:"normal",
                stylingMode:"contained"
            },
            toolbar: 'bottom',
            onClick: () => this.popupVisible = false
        }
    ];

    constructor(
        private taxService: TaxService,
        private priceControlCategoryService: PriceControlCategoryService,
        private stockControlCategoryService: StockControlCategoryService,
        private router: Router) { }

    async ngOnInit() {
        DomHelper.setPageTitle('TAX');
        this.dataSource = this.taxService.getCustomStore();
        this.listPriceControlCategory = (await this.priceControlCategoryService.getList()).data;
        this.listStockControlCategory = (await this.stockControlCategoryService.getList()).data;
    }

    create() {
        this.editMode = 'create';
        this.editResource = new TaxResource();
        this.popupTitle = "Create Tax";
        this.popupVisible = true;
    }

    async update(id) {
        this.editMode = 'update';
        this.editResource = await this.taxService.get(id);

        this.popupTitle = "Update Tax";
        this.popupVisible = true;
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    async save() {
        try {
            this.loadingVisible = true;
    
            if (this.editMode == 'create')
                await this.taxService.create(this.editResource);
            else if (this.editMode == 'update')
                await this.taxService.update(this.editResource);
            
            this.dataGrid.instance.refresh();
            this.popupVisible = false;
            notify('Data saved successfully.', 'success', 2000);
        } catch (error) {
            if ('error' in error) notify(error.error.message, 'error', 3000);
            else notify(error.message, 'error', 3000);
        }

        this.loadingVisible = false;
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "Tax";
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }

    navigateTo(url: string) {
        this.router.navigateByUrl(url);
    }
}
