import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { SupplierCategoryService } from 'src/app/shared/services/supplier-category.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';

@Component({
    selector: 'app-supplier-category',
    templateUrl: './supplier-category.component.html',
    styleUrls: ['./supplier-category.component.scss']
})
export class SupplierCategoryComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: any;
    popupTitle: string;

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode: "contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.dataGrid.instance.saveEditData()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type: "normal",
                stylingMode: "contained"
            },
            toolbar: 'bottom',
            onClick: () => this.dataGrid.instance.cancelEditData()
        }
    ];

    constructor(private supplierCategoryService: SupplierCategoryService) { }

    async ngOnInit() {
        DomHelper.setPageTitle('SUPPLIER CATEGORY');
        this.dataSource = this.supplierCategoryService.getCustomStore();
    }

    create() {
        this.popupTitle = 'Create Supplier Category';
        this.dataGrid.instance.addRow();
    }

    update(id) {
        this.popupTitle = 'Update Supplier Category';
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.editRow(index);
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "SupplierCategory";
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }
}
