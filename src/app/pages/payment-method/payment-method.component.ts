import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { PaymentMethodService } from 'src/app/shared/services/payment-method.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { Helper } from 'src/app/shared/helpers/helper';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';

@Component({
    selector: 'app-unit',
    templateUrl: './payment-method.component.html',
    styleUrls: ['./payment-method.component.scss']
})
export class PaymentMethodComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: any;
    popupTitle: string;

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode: "contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.dataGrid.instance.saveEditData()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type: "normal",
                stylingMode: "contained"
            },
            toolbar: 'bottom',
            onClick: () => this.dataGrid.instance.cancelEditData()
        }
    ];

    constructor(private unitService: PaymentMethodService) { }

    async ngOnInit() {
        DomHelper.setPageTitle('PAYMENT METHOD');
        this.dataSource = this.unitService.getCustomStore();
    }

    create() {
        this.popupTitle = 'Create Payment Method';
        this.dataGrid.instance.addRow();
    }

    update(id) {
        this.popupTitle = 'Update Payment Method';
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.editRow(index);
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "PaymentMethod";
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }
}
