import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { PurchaseOrderService } from 'src/app/shared/services/purchase-order.service';
import { confirm } from 'devextreme/ui/dialog';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { Router } from '@angular/router';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';
import { SupplierService } from 'src/app/shared/services/supplier.service';
import { LocationService } from 'src/app/shared/services/location.service';
import { Helper } from 'src/app/shared/helpers/helper';

@Component({
    selector: 'app-item',
    templateUrl: './purchase-order.component.html',
    styleUrls: ['./purchase-order.component.scss']
})
export class PurchaseOrderComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: CustomStore;

    locationDataSource: CustomStore;
    supplierDataSource: CustomStore;

    filter = {
        supplierId: '',
        locationId: ''
    };

    filterText = "FILTER";
    filterVisible = false;
    toolbarFilter = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'APPLY FILTER',
                icon: 'check',
                stylingMode: "contained",
                width: 150
            },
            toolbar: 'bottom',
            onClick: () => {
                this.dataGrid.instance.refresh();
                this.filterVisible = false;
                this.filterText = "FILTER *";
            }
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'RESET',
                type: "normal",
                stylingMode: "contained"
            },
            toolbar: 'bottom',
            onClick: () => {
                this.filter.supplierId = '';
                this.filter.locationId = '';
                this.dataGrid.instance.refresh();
                this.filterVisible = false;
                this.filterText = "FILTER";
            }
        }
    ];
    showFilter() {
        this.filterVisible = true;
    }

    constructor(
        private purchaseOrderService: PurchaseOrderService,
        private locationService: LocationService,
        private supplierService: SupplierService,
        private router: Router) { }

    async ngOnInit() {
        DomHelper.setPageTitle('PURCHASE ORDER');
        this.dataSource = this.purchaseOrderService.getCustomStore(this.filter);
        this.supplierDataSource = this.supplierService.getCustomStore();
        this.locationDataSource = this.locationService.getCustomStore();
    }

    create() {
        this.router.navigateByUrl(`purchase-order/create`);
    }

    view(id) {
        this.router.navigateByUrl(`purchase-order/view/${id}`);
    }

    async print(id) {
        const result = await this.purchaseOrderService.print(id);
        Helper.printHtml(result.value);
    }

    update(id) {
        this.router.navigateByUrl(`purchase-order/update/${id}`);
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    async open(id, number) {
        const result = await confirm('Are you sure want to re-open ' + number + '?', 'Purchase Order');
        if (result) {
            await this.purchaseOrderService.open(id);
            this.dataGrid.instance.refresh();
        }
    }

    async close(id, number) {
        const result = await confirm('Are you sure want to close ' + number + '?', 'Purchase Order');
        if (result) {
            await this.purchaseOrderService.close(id);
            this.dataGrid.instance.refresh();
        }
    }

    async actionClick(e, data) {
        if (e.itemData == 'View') this.view(data.id);
        else if (e.itemData == 'Print') this.print(data.id);
        else if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
        else if (e.itemData == 'Open') this.open(data.id, data.documentNumber);
        else if (e.itemData == 'Close') this.close(data.id, data.documentNumber);
    }

    navigateTo(url: string) {
        this.router.navigateByUrl(url);
    }
}
