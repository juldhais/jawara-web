import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { PriceControlCategoryService } from 'src/app/shared/services/price-control-category.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';

@Component({
    selector: 'app-price-control-category',
    templateUrl: './price-control-category.component.html',
    styleUrls: ['./price-control-category.component.scss']
})
export class PriceControlCategoryComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: any;
    popupTitle: string;

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode: "contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.dataGrid.instance.saveEditData()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type: "normal",
                stylingMode: "contained"
            },
            toolbar: 'bottom',
            onClick: () => this.dataGrid.instance.cancelEditData()
        }
    ];

    constructor(private priceControlCategoryService: PriceControlCategoryService) { }

    async ngOnInit() {
        DomHelper.setPageTitle('PRICE CONTROL CATEGORY');
        this.dataSource = this.priceControlCategoryService.getCustomStore();
    }

    create() {
        this.popupTitle = 'Create Price Control Category';
        this.dataGrid.instance.addRow();
    }

    update(id) {
        this.popupTitle = 'Update Price Control Category';
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.editRow(index);
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "PriceControlCategory";
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }
}
