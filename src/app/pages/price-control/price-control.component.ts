import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { PriceControlService } from 'src/app/shared/services/price-control.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { PriceControlResource } from 'src/app/shared/resources/price-control.resource';
import { Router } from '@angular/router';
import { PriceControlCategoryResource } from 'src/app/shared/resources/price-control-category.resource';
import { PriceControlCategoryService } from 'src/app/shared/services/price-control-category.service';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';
import { ItemUnitResource } from 'src/app/shared/resources/item-unit.resource';
import { ItemService } from 'src/app/shared/services/item.service';

@Component({
    selector: 'app-item',
    templateUrl: './price-control.component.html',
    styleUrls: ['./price-control.component.scss']
})
export class PriceControlComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: CustomStore;
    searchText: string = '';
    popupTitle: string;
    popupVisible: boolean;
    loadingVisible: boolean;
    editMode: string;
    editResource = new PriceControlResource();

    listItemUnit: ItemUnitResource[] = [];

    itemDataSource: CustomStore;
    listCategory: PriceControlCategoryResource[];

    filter = {
        priceControlCategoryId: '',
        itemId: 'string'
    };

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode:"contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.save()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type:"normal",
                stylingMode:"contained"
            },
            toolbar: 'bottom',
            onClick: () => this.popupVisible = false
        }
    ];

    filterText = "FILTER";
    filterVisible = false;
    toolbarFilter = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'APPLY FILTER',
                icon: 'check',
                stylingMode: "contained",
                width: 150
            },
            toolbar: 'bottom',
            onClick: () => {
                this.dataGrid.instance.refresh();
                this.filterVisible = false;
                this.filterText = "FILTER *";
            }
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'RESET',
                type: "normal",
                stylingMode: "contained"
            },
            toolbar: 'bottom',
            onClick: () => {
                this.filter.priceControlCategoryId = '';
                this.filter.itemId = '';
                this.dataGrid.instance.refresh();
                this.filterVisible = false;
                this.filterText = "FILTER";
            }
        }
    ];

    constructor(
        private priceControlService: PriceControlService,
        private priceControlCategoryService: PriceControlCategoryService,
        private itemService: ItemService,
        private router: Router) { }

    async ngOnInit() {
        DomHelper.setPageTitle('PRICE CONTROL');
        this.dataSource = this.priceControlService.getCustomStore(this.filter);
        this.itemDataSource = this.itemService.getLookupCustomStore('sellable');
        this.listCategory = (await this.priceControlCategoryService.getList()).data;
    }

    async itemChange() {
        let item = await this.itemService.get(this.editResource.itemId);
        this.listItemUnit = this.itemService.getUnits(item);
        this.editResource.unit = item.unit;
        this.editResource.ratio = 1;
    }

    async unitChange() {
        let unit = this.listItemUnit.filter(x => x.unit == this.editResource.unit)[0];
        this.editResource.ratio = unit ? unit.ratio : 1;
    }

    create() {
        this.editMode = 'create';
        this.editResource = new PriceControlResource();
        this.listItemUnit = [];
        this.popupTitle = "Create Price Control";
        this.popupVisible = true;
    }

    async update(id) {
        this.editMode = 'update';
        this.editResource = await this.priceControlService.get(id);

        let item = await this.itemService.get(this.editResource.itemId);
        this.listItemUnit = this.itemService.getUnits(item);

        this.popupTitle = "Update Price Control";
        this.popupVisible = true;
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    async save() {
        try {
            this.loadingVisible = true;
    
            if (this.editMode == 'create')
                await this.priceControlService.create(this.editResource);
            else if (this.editMode == 'update')
                await this.priceControlService.update(this.editResource);
            
            this.dataGrid.instance.refresh();
            this.popupVisible = false;
            notify('Data saved successfully.', 'success', 2000);
        } catch (error) {
            if ('error' in error) notify(error.error.message, 'error', 3000);
            else notify(error.message, 'error', 3000);
        }

        this.loadingVisible = false;
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "PriceControl";
    }

    showFilter() {
        this.filterVisible = true;
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }

    navigateTo(url: string) {
        this.router.navigateByUrl(url);
    }
}
