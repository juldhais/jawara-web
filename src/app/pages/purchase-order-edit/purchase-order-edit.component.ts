import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DxDataGridComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import { confirm } from 'devextreme/ui/dialog';
import notify from 'devextreme/ui/notify';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';
import { Helper, UserPrivilege } from 'src/app/shared/helpers/helper';
import { ItemUnitResource } from 'src/app/shared/resources/item-unit.resource';
import { ItemResource } from 'src/app/shared/resources/item.resource';
import { LookupResource } from 'src/app/shared/resources/lookup.resource';
import { PurchaseOrderDetailResource, PurchaseOrderResource } from 'src/app/shared/resources/purchase-order.resource';
import { TaxResource } from 'src/app/shared/resources/tax.resource';
import { ItemService } from 'src/app/shared/services/item.service';
import { LocationService } from 'src/app/shared/services/location.service';
import { PurchaseOrderService } from 'src/app/shared/services/purchase-order.service';
import { SupplierService } from 'src/app/shared/services/supplier.service';
import { TaxService } from 'src/app/shared/services/tax.service';

@Component({
    selector: 'app-purchase-order-edit',
    templateUrl: './purchase-order-edit.component.html',
    styleUrls: ['./purchase-order-edit.component.scss']
})
export class PurchaseOrderEditComponent implements OnInit {
    @ViewChild('dataGridDetail', { static: false })
    dataGridDetail: DxDataGridComponent;

    id: string;
    mode: string;
    popupTitle: string;
    popupVisible: boolean;
    loadingVisible: boolean;
    detailEditMode: string;

    editResource = new PurchaseOrderResource();
    editDetail = new PurchaseOrderDetailResource();
    detailItem: ItemResource;
    detailUnits: ItemUnitResource[] = [];
    detailVat = new TaxResource();
    detailIct = new TaxResource();
    detailOth = new TaxResource();

    supplierDataSource: CustomStore;
    locationDataSource: CustomStore;
    itemDataSource: CustomStore;
    listTax: TaxResource[] = [];
    listVat: LookupResource[] = [];
    listIct: LookupResource[] = [];
    listOth: LookupResource[] = [];

    privilege: UserPrivilege;

    scanCodeValue: string;

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode:"contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.saveDetail()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type:"normal",
                stylingMode:"contained"
            },
            toolbar: 'bottom',
            onClick: () => this.popupVisible = false
        }
    ];

    constructor(private purchaseOrderService: PurchaseOrderService,
        private supplierService: SupplierService,
        private locationService: LocationService,
        private itemService: ItemService,
        private taxService: TaxService,
        private router: Router,
        private route: ActivatedRoute) { }

    async ngOnInit() {
        this.privilege = Helper.getUserPrivilege();
        
        this.supplierDataSource = this.supplierService.getLookupCustomStore();
        this.locationDataSource = this.locationService.getLookupCustomStore();
        this.itemDataSource = this.itemService.getLookupCustomStore('purchasable');
        this.listTax = (await this.taxService.getList()).data;
        this.listVat = await this.taxService.getLookup({ type: 'Value Added Tax' });
        this.listIct = await this.taxService.getLookup({ type: 'Income Tax' });
        this.listOth = await this.taxService.getLookup({ type: 'Other Tax' });

        this.route.params.subscribe(async params => {
            this.mode = params['mode'];

            if (this.mode == 'create') {
                this.editResource = new PurchaseOrderResource();
                this.editResource.documentDate = new Date();
                this.editResource.status = 'Open';
                this.editResource.locationId = Helper.getIdentity().user.defaultLocationId;
                DomHelper.setPageTitle('CREATE PURCHASE ORDER');
            }
            else if (this.mode == 'update') {
                this.id = params['id'];
                this.editResource = await this.purchaseOrderService.get(this.id);
                DomHelper.setPageTitle('UPDATE PURCHASE ORDER');
            }
            else if (this.mode == 'view') {
                this.id = params['id'];
                this.editResource = await this.purchaseOrderService.get(this.id);
                DomHelper.setPageTitle('VIEW PURCHASE ORDER');
            }
            else window.history.back();
        });
    }

    async itemChange() {
        if (!this.editDetail.itemId) return;

        this.detailItem = await this.itemService.get(this.editDetail.itemId);
        this.detailUnits = this.itemService.getUnits(this.detailItem);
        this.editDetail.unit = this.detailItem.unit;
        this.editDetail.ratio = 1;
        this.editDetail.price = this.detailItem.purchasePrice;
        this.calculateDetail();
    }

    async unitChange() {
        const unit = this.detailUnits.filter(x => x.unit == this.editDetail.unit)[0];
        if (unit) {
            this.editDetail.ratio = unit.ratio;
            this.editDetail.price = this.detailItem.purchasePrice * this.editDetail.ratio;
            this.calculateDetail();
        }
    }

    calculateDetail() {
        this.editDetail.subtotalBeforeTax = this.editDetail.quantity * this.editDetail.price 
            * (100 - this.editDetail.discountPercent1) / 100
            * (100 - this.editDetail.discountPercent2) / 100
            - this.editDetail.discountAmount;
        
        if (this.editDetail.valueAddedTaxId) {
            let vat = this.listTax.filter(x => x.id == this.editDetail.valueAddedTaxId)[0];
            this.editDetail.valueAddedTaxAmount = this.editDetail.subtotalBeforeTax * vat.rate / 100 + vat.amount;
        }
        if (this.editDetail.incomeTaxId) {
            let ict = this.listTax.filter(x => x.id == this.editDetail.incomeTaxId)[0];
            this.editDetail.incomeTaxAmount = this.editDetail.subtotalBeforeTax * ict.rate / 100 + ict.amount;
        }
        if (this.editDetail.otherTaxId) {
            let oth = this.listTax.filter(x => x.id == this.editDetail.otherTaxId)[0];
            this.editDetail.otherTaxAmount = this.editDetail.subtotalBeforeTax * oth.rate / 100 + oth.amount;
        }

        this.editDetail.subtotalAfterTax = this.editDetail.subtotalBeforeTax 
            + this.editDetail.valueAddedTaxAmount
            + this.editDetail.incomeTaxAmount
            + this.editDetail.otherTaxAmount;
    }

    calculateTotal() {
        let totalBeforeTax = 0;
        let totalValueAddedTax = 0;
        let totalIncomeTax = 0;
        let totalOtherTax = 0;
        let totalAfterTax = 0;

        for (const detail of this.editResource.details) {
            totalBeforeTax += detail.subtotalBeforeTax;
            totalValueAddedTax += detail.valueAddedTaxAmount;
            totalIncomeTax += detail.incomeTaxAmount;
            totalOtherTax += detail.otherTaxAmount;
            totalAfterTax += detail.subtotalAfterTax;
        }

        this.editResource.totalBeforeTax = totalBeforeTax;
        this.editResource.totalValueAddedTax = totalValueAddedTax;
        this.editResource.totalIncomeTax = totalIncomeTax;
        this.editResource.totalOtherTax = totalOtherTax;
        this.editResource.totalAfterTax = totalAfterTax;
    }

    actionClick(e, data) {
        if (e.itemData == 'Update') this.updateDetail(data);
        else if (e.itemData == 'Delete') this.deleteDetail(data);
    }

    createDetail() {
        this.detailEditMode = 'create';
        this.popupTitle = 'Add Item';
        
        this.editDetail = new PurchaseOrderDetailResource();

        let sequence = 0;
        if (this.editResource.details.length > 0)
            sequence = Math.max(...this.editResource.details.map(x => x.sequence));
        this.editDetail.sequence = sequence + 1;

        this.editDetail.quantity = 1;

        this.popupVisible = true;
    }

    async updateDetail(data: PurchaseOrderDetailResource) {
        this.detailEditMode = 'update';
        this.popupTitle = 'Update Item';

        this.editDetail = JSON.parse(JSON.stringify(data));

        this.detailItem = await this.itemService.get(this.editDetail.itemId);
        this.detailUnits = this.itemService.getUnits(this.detailItem);

        this.popupVisible = true;
    }

    async deleteDetail(data: PurchaseOrderDetailResource) {
        const result = await confirm('Are you sure?', 'Delete Item');
        if (result) {
            const index = this.editResource.details.findIndex(x => x.sequence == data.sequence);
            this.editResource.details.splice(index, 1);
        }
    }

    async scanCode(e) {
        if (e.event.key == 'Enter' && this.scanCodeValue) {
            
            this.detailItem = await this.itemService.getByCode(this.scanCodeValue);
            
            if (this.detailItem) {
                this.editDetail = this.editResource.details
                    .filter(x => x.itemId == this.detailItem.id
                        && x.unit == this.detailItem.unit
                        && x.ratio == 1)[0];
                    
                if (this.editDetail) {
                    this.editDetail.quantity += 1;
                    this.calculateDetail();
                }
                else {
                    this.editDetail = new PurchaseOrderDetailResource();
    
                    let sequence = 0;
                    if (this.editResource.details.length > 0)
                        sequence = Math.max(...this.editResource.details.map(x => x.sequence));
                    this.editDetail.sequence = sequence + 1;
            
                    this.editDetail.itemCode = this.detailItem.code;
                    this.editDetail.itemDescription = this.detailItem.description;
                    this.editDetail.itemId = this.detailItem.id;
                    this.editDetail.unit = this.detailItem.unit;
                    this.editDetail.ratio = 1;
                    this.editDetail.quantity = 1;
                    this.editDetail.price = this.detailItem.purchasePrice;

                    this.calculateDetail();

                    const detail = JSON.parse(JSON.stringify(this.editDetail));
                    this.editResource.details.push(detail);
                }

                this.calculateTotal();
            }

            this.scanCodeValue = '';
        }
    }

    saveDetail() {
        try {
            if (!this.editDetail.itemId)
            throw new Error('Item cannot be empty.');

            if (this.editDetail.quantity == 0)
                throw new Error('Quantity cannot be empty.');

            this.editDetail.itemCode = this.detailItem.code;
            this.editDetail.itemDescription = this.detailItem.description;
            this.calculateDetail();

            if (this.detailEditMode == 'create') {
                const detail = new PurchaseOrderDetailResource();
                Helper.copyObject(detail, this.editDetail);
                this.editResource.details.push(detail);
            }
            else if (this.detailEditMode == 'update') {
                let detail = this.editResource.details.filter(x => x.sequence == this.editDetail.sequence)[0];
                Helper.copyObject(detail, this.editDetail);
            }

            this.calculateTotal();

            this.popupVisible = false;
        } catch (error) {
            notify(error.message, 'error', 3000);
        }
    }

    async save() {
        try {
            this.loadingVisible = true;

            if (this.mode == 'create')
                await this.purchaseOrderService.create(this.editResource);
            else if (this.mode == 'update')
                await this.purchaseOrderService.update(this.editResource);

            notify('Data saved successfully.', 'success', 2000);

            this.router.navigateByUrl('purchase-order');
        } catch (error) {
            if ('error' in error) notify(error.error.message, 'error', 3000);
            else notify(error.message, 'error', 3000);
        }

        this.loadingVisible = false;
    }

    async cancel() {
        const result = await confirm('Are you sure want to cancel?', 'Purchase Order');
        if (result) this.router.navigateByUrl('purchase-order');
    }

    back() {
        window.history.back();
    }
}
