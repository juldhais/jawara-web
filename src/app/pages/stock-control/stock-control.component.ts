import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { StockControlService } from 'src/app/shared/services/stock-control.service';
import notify from 'devextreme/ui/notify';
import CustomStore from 'devextreme/data/custom_store';
import { StockControlResource } from 'src/app/shared/resources/stock-control.resource';
import { Router } from '@angular/router';
import { StockControlCategoryResource } from 'src/app/shared/resources/stock-control-category.resource';
import { StockControlCategoryService } from 'src/app/shared/services/stock-control-category.service';
import { DomHelper } from 'src/app/shared/helpers/dom-helper';
import { ItemUnitResource } from 'src/app/shared/resources/item-unit.resource';
import { ItemService } from 'src/app/shared/services/item.service';

@Component({
    selector: 'app-item',
    templateUrl: './stock-control.component.html',
    styleUrls: ['./stock-control.component.scss']
})
export class StockControlComponent implements OnInit {
    @ViewChild('dataGrid', { static: false })
    dataGrid: DxDataGridComponent;

    dataSource: CustomStore;
    searchText: string = '';
    popupTitle: string;
    popupVisible: boolean;
    loadingVisible: boolean;
    editMode: string;
    editResource = new StockControlResource();

    listItemUnit: ItemUnitResource[] = [];

    itemDataSource: CustomStore;
    listCategory: StockControlCategoryResource[];

    filter = {
        stockControlCategoryId: '',
        itemId: 'string'
    };

    toolbarItems = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'SAVE',
                icon: 'check',
                stylingMode:"contained",
                width: 100
            },
            toolbar: 'bottom',
            onClick: () => this.save()
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'CANCEL',
                type:"normal",
                stylingMode:"contained"
            },
            toolbar: 'bottom',
            onClick: () => this.popupVisible = false
        }
    ];

    filterText = "FILTER";
    filterVisible = false;
    toolbarFilter = [
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'APPLY FILTER',
                icon: 'check',
                stylingMode: "contained",
                width: 150
            },
            toolbar: 'bottom',
            onClick: () => {
                this.dataGrid.instance.refresh();
                this.filterVisible = false;
                this.filterText = "FILTER *";
            }
        },
        {
            widget: 'dxButton',
            location: 'after',
            options: {
                text: 'RESET',
                type: "normal",
                stylingMode: "contained"
            },
            toolbar: 'bottom',
            onClick: () => {
                this.filter.stockControlCategoryId = null;
                this.filter.itemId = null;
                this.dataGrid.instance.refresh();
                this.filterVisible = false;
                this.filterText = "FILTER";
            }
        }
    ];

    constructor(
        private stockControlService: StockControlService,
        private stockControlCategoryService: StockControlCategoryService,
        private itemService: ItemService,
        private router: Router) { }

    async ngOnInit() {
        DomHelper.setPageTitle('STOCK CONTROL');
        this.dataSource = this.stockControlService.getCustomStore(this.filter);
        this.itemDataSource = this.itemService.getLookupCustomStore('stockable');
        this.listCategory = (await this.stockControlCategoryService.getList()).data;
    }

    async itemChange() {
        let item = await this.itemService.get(this.editResource.itemId);
        this.listItemUnit = this.itemService.getUnits(item);
    }

    create() {
        this.editMode = 'create';
        this.editResource = new StockControlResource();
        this.listItemUnit = [];
        this.popupTitle = "Create Stock Control";
        this.popupVisible = true;
    }

    async update(id) {
        this.editMode = 'update';
        this.editResource = await this.stockControlService.get(id);

        let item = await this.itemService.get(this.editResource.itemId);
        this.listItemUnit = this.itemService.getUnits(item);

        this.popupTitle = "Update Stock Control";
        this.popupVisible = true;
    }

    async delete(id) {
        const index = this.dataGrid.instance.getRowIndexByKey(id);
        this.dataGrid.instance.deleteRow(index);
    }

    async save() {
        try {
            this.loadingVisible = true;
    
            if (this.editMode == 'create')
                await this.stockControlService.create(this.editResource);
            else if (this.editMode == 'update')
                await this.stockControlService.update(this.editResource);
            
            this.dataGrid.instance.refresh();
            this.popupVisible = false;
            notify('Data saved successfully.', 'success', 2000);
        } catch (error) {
            if ('error' in error) notify(error.error.message, 'error', 3000);
            else notify(error.message, 'error', 3000);
        }

        this.loadingVisible = false;
    }

    export() {
        this.dataGrid.instance.exportToExcel(false);
    }

    onFileSaving(e) {
        e.fileName = "StockControl";
    }

    showFilter() {
        this.filterVisible = true;
    }

    async actionClick(e, data) {
        if (e.itemData == 'Update') this.update(data.id);
        else if (e.itemData == 'Delete') this.delete(data.id);
    }

    navigateTo(url: string) {
        this.router.navigateByUrl(url);
    }
}
