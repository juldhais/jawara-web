import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SideNavOuterToolbarModule, SideNavInnerToolbarModule, SingleCardModule } from './layouts';
import { FooterModule, LoginFormModule } from './shared/components';
import { AuthService, ScreenService, AppInfoService } from './shared/services';
import { UnauthenticatedContentModule } from './unauthenticated-content';
import { HttpInterceptorService } from './shared/services/http-interceptor.service';
import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        BrowserModule,
        SideNavOuterToolbarModule,
        SideNavInnerToolbarModule,
        SingleCardModule,
        FooterModule,
        LoginFormModule,
        UnauthenticatedContentModule,
        AppRoutingModule
    ],
    providers: [
        AuthService, 
        ScreenService,
        AppInfoService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpInterceptorService,
            multi: true
        },
        { provide: LocationStrategy, useClass: HashLocationStrategy  },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
